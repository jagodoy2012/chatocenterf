import 'package:chatocenter/src/bloc/provider.dart';
import 'package:chatocenter/src/pages/BottomNavBar.dart';
import 'package:chatocenter/src/pages/login_page.dart';
import 'package:chatocenter/src/pages/numero_page.dart';
import 'package:chatocenter/src/pages/producto_page.dart';
import 'package:chatocenter/src/pages/productos_page.dart';
import 'package:chatocenter/src/pages/usuario_page.dart';
import 'package:chatocenter/src/pages/usuarios_page.dart';
import 'package:chatocenter/src/preferencias_usuario/preferencias_usuario.dart';
import 'package:flutter/material.dart';


 
void main() async {


  WidgetsFlutterBinding.ensureInitialized(); // para grabar en storage
  final prefs = new PreferenciasUsuario();
  await prefs.initPrefs();
  runApp(MyApp());

}
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
     
    return Provider(
          child: MaterialApp(
        debugShowCheckedModeBanner:  false,
        title: 'Material App',
        initialRoute: 'login',
        routes: {
      
           'BottomNavBar': (BuildContext context) => BottomNavBar(),
           'productos': (BuildContext context) => ProductosPage(),
           'producto': (BuildContext context) => ProductoPage(),
           'tel': (BuildContext context) => NumeroPage(),
           'usuarios': (BuildContext context) => UsuariosPage(),
           'usuario': (BuildContext context) => UsuarioPage(),
           'login' : (BuildContext context) => LoginPage(),
          
          
            
        },
      ),
    );
  }
}