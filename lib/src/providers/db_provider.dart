


import 'dart:io';


import 'package:chatocenter/src/models/ventasdb_model.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DBProvider{
 
static Database _database; // instancia de base de datos 

static final DBProvider db = DBProvider._(); // constructor privado para que no se re inicie  
DBProvider._();


Future<Database> get database async {

if(_database != null)return _database;

else{

  _database =  await initDB();

  return _database;
}

}


initDB() async{  // crear una base de datos 

Directory documentsDirectory  = await getApplicationDocumentsDirectory();

final path = join(documentsDirectory.path,'Ventasdet.db');

//comando para crear bse de datos


return await openDatabase(
  path,
  version: 2, 
  onOpen: (db) {}, 
  onCreate: (Database db, int version)async{
    //se crean todas las tablas 
    await db.execute(
      'CREATE TABLE detalle ('
      ' id INTEGER PRIMARY KEY AUTOINCREMENT,'
      ' producto TEXT,'
      ' comentario TEXT,' 
      ' id_productos INTEGER,'
      ' cantidad INTEGER,'
      ' precio DOUBLE '
      ')'
    ); 
  }
  );


} 


// crear registro 
nuevaVentaRaw(Ventasdb nuevaVenta)async{

  final db = await database; 
  final res = await db.rawInsert(
    'INSERT Into detalle (produ, id_productos, comentario, cantidad, precio) '// lleva el espacio al final 
    'VALUES ( ${nuevaVenta.producto}, ${nuevaVenta.idProductos}, ${nuevaVenta.comentario}, ${nuevaVenta.cantidad}, ${nuevaVenta.precio})' // CUANOD ES STRING VA ENTRE APOSTROFES
  );
  return res ; 

}

nuevaVenta(Ventasdb nuevaVenta)async{

  final db = await database;
  final res = db.insert('detalle', nuevaVenta.toJson());
  return res;


}


Future<List<Ventasdb>> getTodasVentas()async{

final db = await database;
final res = await db.query('detalle');

List<Ventasdb> list = res.isNotEmpty 
                                ? res.map((v) => Ventasdb.fromJson(v)).toList()
                                :[];
                                
                                return list;
}

Future<int> deleteVentas(int id)async{

  final db = await database; 
  final res = await db.rawDelete('DELETE FROM detalle WHERE id = ?', [id]);
  return res;
 

}
Future<int> deleteallVentas()async{

  final db = await database;
  final res = await db.rawDelete('DELETE FROM detalle');
  return res;


}

}