

import 'dart:convert';
import 'dart:io'; 



import 'package:chatocenter/src/models/productos_model.dart';
import 'package:http/http.dart' as http;
import 'package:mime_type/mime_type.dart';
import 'package:http_parser/http_parser.dart';
class ProductosProvider{
 
  final String _url = 'https://foxylabs-ventas.herokuapp.com/api/productos';
  final String _urlest = 'https://foxylabs-ventas.herokuapp.com/api/productos/status_1'; 
  final String _urlest1 = 'https://foxylabs-ventas.herokuapp.com/api/productos/status_2'; 
  final  Map<String, String> headers = {
"Content-Type": "application/x-www-form-urlencoded",  
"Content-type": "application/json"};


 Future<bool> crearProductos(ProductosModel productos)async{
   final url = '$_url/add';
  

  if(productos.img!="" && productos.img!=" " ){

  }
  else
  {
    productos.img =" ";
  }

 final resp = await http.post(url, headers: headers, body: productosModelToJson(productos));
 
  final decodedData = json.decode(resp.body);
  return true;
  }


Future<List<ProductosModel>> cargarProductos() async{

  final url = '$_urlest';
  final resp = await http.get(url);

  final List<dynamic> decodedData = json.decode(resp.body);
  final List<ProductosModel> productos  = new List();
  if(decodedData == null) return[];



  decodedData.forEach((producto) {
    final catTemp = ProductosModel.fromJson(producto);
    productos.add(catTemp);
   
  });

  return productos;

}
Future<List<ProductosModel>> cargarProductosext() async{

  final url = '$_urlest1';
  final resp = await http.get(url);

  final List<dynamic> decodedData = json.decode(resp.body);
  final List<ProductosModel> productos  = new List();
  if(decodedData == null) return[];



  decodedData.forEach((producto) {
    final catTemp = ProductosModel.fromJson(producto);
    productos.add(catTemp);
   
  });

  return productos;

}

Future<bool> borrarCategoria(ProductosModel productos )async{

int id = productos.id;

final url = '$_url/update_$id';



productos.estado = "0";


final resp = await http.put(url, headers: headers, body: productosModelToJson(productos));
 
final decodedData  = json.decode(resp.body);



return true;

}



Future<bool> modificarProductos(ProductosModel productos )async{

int id = productos.id;

final url = '$_url/update_$id';



final resp = await http.put(url, headers: headers, body: productosModelToJson(productos));
 
final decodedData  = json.decode(resp.body);



return true;

} 


Future<String> subirImagen(File imagen)async {

  final url = Uri.parse('https://api.cloudinary.com/v1_1/dfe2fyrrb/image/upload?upload_preset=kcwihw9b');
  final mimeType = mime(imagen.path).split('/');


  final imageUploadRequest = http.MultipartRequest(
    'POST',
    url
  );

  final file = await http.MultipartFile.fromPath(
    'file', 
    imagen.path,
    contentType: MediaType( mimeType[0], mimeType[1] )
    );

    imageUploadRequest.files.add(file);//si copiamos muchas veces podemos adjuntar varios 

    final streamResponse = await imageUploadRequest.send();
    final resp = await http.Response.fromStream(streamResponse);

    if(resp.statusCode !=200 && resp.statusCode !=  201){
      return null;
    }

    final respData = json.decode(resp.body);
    return respData['secure_url'];

}


}