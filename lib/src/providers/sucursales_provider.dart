  

import 'dart:convert';
import 'dart:io'; 




import 'package:chatocenter/src/models/sucursales_model.dart';
import 'package:http/http.dart' as http;
import 'package:mime_type/mime_type.dart';
import 'package:http_parser/http_parser.dart';
class SucursalesProvider{
 
  final String _url = 'https://mototaxy.herokuapp.com/api/taxis_usuarios_direcciones/type_2';
  final  Map<String, String> headers = {
"Content-Type": "application/x-www-form-urlencoded",  
"Content-type": "application/json"};


  

  
Future<List<Sucursales>> cargarSucursales() async{

  final url = '$_url';
  final resp = await http.get(url); 
  final List<dynamic> decodedData = json.decode(resp.body);
  final List<Sucursales> sucursales  = new List();
  if(decodedData == null) return[]; 

 

  decodedData.forEach((sucursal) {
    final catTemp = Sucursales.fromJson(sucursal);
    sucursales.add(catTemp);
   
  });

  return sucursales;

}





















}