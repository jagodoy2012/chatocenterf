  

import 'dart:convert';
import 'dart:io'; 



import 'package:chatocenter/src/models/pedidos_model.dart';
import 'package:chatocenter/src/models/usuarios_model.dart';
import 'package:chatocenter/src/models/usuariosnpass_modal.dart';
import 'package:http/http.dart' as http;
import 'package:mime_type/mime_type.dart';
import 'package:http_parser/http_parser.dart';
class PedidosProvider{
 
  final String _url = 'https://foxylabs-ventas.herokuapp.com/api/mototaxys';
  final  Map<String, String> headers = {
"Content-Type": "application/x-www-form-urlencoded",  
"Content-type": "application/json"};



Future<List<PedidosCliente>> cargarTelUsuarios(int cel) async{

  int cel1 = cel; 
  final url = '$_url/cel_$cel1';

  final resp = await http.get(url, headers: headers);
 // final resp = await http.get(url);
 
  final List<dynamic> decodedData = json.decode(resp.body);
  
  final List<PedidosCliente> pedidos  = new List();


  if(decodedData == null) return[]; 

 

  decodedData.forEach((pedido) {
    final catTemp = PedidosCliente.fromJson(pedido);
    pedidos.add(catTemp);
   
  });

  return pedidos;

}











}