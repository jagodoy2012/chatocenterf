  

import 'dart:convert';
import 'dart:io'; 


import 'package:chatocenter/src/models/usuariosdirecciones_model.dart';
import 'package:chatocenter/src/preferencias_usuario/preferencias_usuario.dart';
import 'package:http/http.dart' as http;
import 'package:mime_type/mime_type.dart';
import 'package:http_parser/http_parser.dart';
class UsuariosDireccionesProvider{
 final _prefs = new PreferenciasUsuario();
  final String _url = 'https://foxylabs-ventas.herokuapp.com/api/usuarios_direcciones';
  final  Map<String, String> headers = {
"Content-Type": "application/x-www-form-urlencoded",  
"Content-type": "application/json"};
 

 Future<bool> crearUsuariosDirecciones(UsuariosDireccionesModel usuarios)async{
  _prefs.returndir = 0;
  
       usuarios.idUsuarios =    _prefs.idcliente; 
   final url = '$_url/add';
 

 
 
 final resp = await http.post(url, headers: headers, body: usuariosDireccionesModelToJson(usuarios));
 
  final decodedData = json.decode(resp.body);
  print(decodedData);
  print("sin necesidad");
  

  return true;
  }


Future<List<UsuariosDireccionesModel>> cargarUsuariosDirecciones() async{

  final url = '$_url/get_all';
  final resp = await http.get(url);

  final List<dynamic> decodedData = json.decode(resp.body);
  final List<UsuariosDireccionesModel> usuarios  = new List();
  if(decodedData == null) return[];

 

  decodedData.forEach((usuario) {
    final catTemp = UsuariosDireccionesModel.fromJson(usuario);
    usuarios.add(catTemp);
   
  });

  return usuarios;

}


Future<bool> borrarUsuariosDirecciones(UsuariosDireccionesModel usuarios )async{

int id = usuarios.id;

final url = '$_url/update_$id';



usuarios.estado = "0";


final resp = await http.put(url, headers: headers, body: usuariosDireccionesModelToJson(usuarios));
 
final decodedData  = json.decode(resp.body);



return true;

}



Future<bool> modificarUsuariosDirecciones(UsuariosDireccionesModel usuarios )async{

int id = usuarios.id;

final url = '$_url/update_$id';



final resp = await http.put(url, headers: headers, body: usuariosDireccionesModelToJson(usuarios));
 
final decodedData  = json.decode(resp.body);



return true;

} 


Future<String> subirImagen(File imagen)async {

  final url = Uri.parse('https://api.cloudinary.com/v1_1/dfe2fyrrb/image/upload?upload_preset=kcwihw9b');
  final mimeType = mime(imagen.path).split('/');


  final imageUploadRequest = http.MultipartRequest(
    'POST',
    url
  );

  final file = await http.MultipartFile.fromPath(
    'file', 
    imagen.path,
    contentType: MediaType( mimeType[0], mimeType[1] )
    );

    imageUploadRequest.files.add(file);//si copiamos muchas veces podemos adjuntar varios 

    final streamResponse = await imageUploadRequest.send();
    final resp = await http.Response.fromStream(streamResponse);

    if(resp.statusCode !=200 && resp.statusCode !=  201){
      return null;
    }

    final respData = json.decode(resp.body);
    return respData['secure_url'];

}






Future<List<UsuariosDireccionesModel>> cargarUsuariosDireccionesid(String iddir) async{
 
  final url = '$_url/user_$iddir';
   final resp = await http.get(url, headers: headers);
 

  final List<dynamic> decodedData = json.decode(resp.body);
  final List<UsuariosDireccionesModel> usuarios  = new List();
  if(decodedData == null) return[];

 

  decodedData.forEach((usuario) {
    final catTemp = UsuariosDireccionesModel.fromJson(usuario);
    usuarios.add(catTemp);
   
  });

  return usuarios;

}





}