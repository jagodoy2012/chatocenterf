// To parse this JSON data, do
//
//     final ventasdb = ventasdbFromJson(jsonString);

import 'dart:convert';

Ventasdb ventasdbFromJson(String str) => Ventasdb.fromJson(json.decode(str));

String ventasdbToJson(Ventasdb data) => json.encode(data.toJson());

class Ventasdb {
    int id;
    int idProductos;
    String producto;
    int cantidad;
    String comentario;
    double precio;
    String descripcion;
   

    Ventasdb({
        this.id,
        this.idProductos = 0,
        this.producto = " ",
        this.cantidad = 0,
        this.comentario = " ",
        this.precio = 0.00,
       
     
    });

    factory Ventasdb.fromJson(Map<String, dynamic> json) => Ventasdb(
        id         : json["id"],
        idProductos: json["id_productos"],
        producto: json["producto"],
        cantidad: json["cantidad"],
        comentario: json['comentario'],
        precio: json["precio"].toDouble(),
        
     
    );

    Map<String, dynamic> toJson() => {
        "id"          : id,
        "id_productos": idProductos,
        "producto": producto,
        "cantidad": cantidad,
        "comentario": comentario,
        "precio": precio,
        
       
    };
}
