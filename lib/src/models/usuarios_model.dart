// To parse this JSON data, do
//
//     final usuariosModel = usuariosModelFromJson(jsonString);

import 'dart:convert';

UsuariosModel usuariosModelFromJson(String str) => UsuariosModel.fromJson(json.decode(str));

String usuariosModelToJson(UsuariosModel data) => json.encode(data.toJson());

class UsuariosModel {
    int id;
    String nombre;
    String apellido;
    String correo;
    String email;
    String celular;
    String telefono;
    int idUsuariosTipos;
    String fb;
    String inst;
    String ubicacion;
    String descripcion;
    String password;
    String estado;

    UsuariosModel({
        this.id,
        this.nombre = "",
        this.apellido = "",
        this.correo = "",
        this.email = "",
        this.celular = "",
        this.telefono = "",
        this.idUsuariosTipos = 1,
        this.fb = "",
        this.inst  = "",
        this.ubicacion = " ",
        this.descripcion  = "",
        this.password  = "123foragro",
        this.estado  = "1", 
    });

    factory UsuariosModel.fromJson(Map<String, dynamic> json) => UsuariosModel(
        id: json["id"],
        nombre: json["nombre"],
        apellido: json["apellido"],
        correo: json["correo"],
        email: json["email"],
        celular: json["celular"],
        telefono: json["telefono"],
        idUsuariosTipos: json["id_usuarios_tipos"],
        fb: json["fb"],
        inst: json["inst"],
        ubicacion: json["ubicacion"],
        descripcion: json["descripcion"],
        password: json["password"],
        estado: json["estado"],
    );

    Map<String, dynamic> toJson() => { 
        "id": id,
        "nombre": nombre,
        "apellido": apellido,
        "correo": correo,
        "email": email,
        "celular": celular,
        "telefono": telefono,
        "id_usuarios_tipos": idUsuariosTipos,
        "fb": fb,
        "inst": inst,
        "ubicacion": ubicacion,
        "descripcion": descripcion,
        "password": password,
        "estado": estado,
    };
}