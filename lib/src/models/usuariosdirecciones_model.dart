// To parse this JSON data, do
//
//     final usuariosDireccionesModel = usuariosDireccionesModelFromJson(jsonString);

import 'dart:convert';

UsuariosDireccionesModel usuariosDireccionesModelFromJson(String str) => UsuariosDireccionesModel.fromJson(json.decode(str));

String usuariosDireccionesModelToJson(UsuariosDireccionesModel data) => json.encode(data.toJson());

class UsuariosDireccionesModel {
    int id;
    int idUsuarios;
    String direccion;
    String estado;
    int id_colonia_sucursal;

    UsuariosDireccionesModel({
        this.id,
        this.idUsuarios = 0,
        this.direccion = "",
        this.estado = "1",
        this.id_colonia_sucursal = 0,
    });

    factory UsuariosDireccionesModel.fromJson(Map<String, dynamic> json) => UsuariosDireccionesModel(
        id: json["id"],
        idUsuarios: json["id_usuarios"],
        direccion: json["direccion"],
        estado: json["estado"],
        id_colonia_sucursal: json["id_colonia_sucursal"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "id_usuarios": idUsuarios,
        "direccion": direccion,
        "estado": estado,
        "id_colonia_sucursal": id_colonia_sucursal,
    };
}