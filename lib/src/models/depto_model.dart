// To parse this JSON data, do
//
//     final deptoModel = deptoModelFromJson(jsonString);

import 'dart:convert';

DeptoModel deptoModelFromJson(String str) => DeptoModel.fromJson(json.decode(str));

String deptoModelToJson(DeptoModel data) => json.encode(data.toJson());

class DeptoModel {
    int id;
    String nombre;
    String descripcion;
    int estado;

    DeptoModel({
        this.id ,
        this.nombre = "",
        this.descripcion = "",
        this.estado = 1,
    });

    factory DeptoModel.fromJson(Map<String, dynamic> json) => DeptoModel(
        id: json["id"],
        nombre: json["nombre"],
        descripcion: json["descripcion"],
        estado: json["estado"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "nombre": nombre,
        "descripcion": descripcion,
        "estado": estado,
    };
}
