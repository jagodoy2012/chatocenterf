// To parse this JSON data, do
//
//     final productosModel = productosModelFromJson(jsonString);

import 'dart:convert';

ProductosModel productosModelFromJson(String str) => ProductosModel.fromJson(json.decode(str));

String productosModelToJson(ProductosModel data) => json.encode(data.toJson());

class ProductosModel {
    int id;
    int idProductos;
    String titulo;
    String descripcion;
    double precio;
    String img;
    String estado;

    ProductosModel({
        this.id,
        this.idProductos = 0,
        this.titulo = "",
        this.descripcion= "",
        this.precio=0.00,
        this.img = " ",
        this.estado = "",
    });

    factory ProductosModel.fromJson(Map<String, dynamic> json) => ProductosModel(
        id: json["id"],
        idProductos: json["id_productos"],
        titulo: json["titulo"],
        descripcion: json["descripcion"],
        precio: json["precio"].toDouble(),
        img: json["img"],
        estado: json["estado"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "id_productos": idProductos,
        "titulo": titulo,
        "descripcion": descripcion,
        "precio": precio,
        "img": img,
        "estado": estado,
    };
}
