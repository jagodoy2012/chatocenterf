// To parse this JSON data, do
// 
//     final sucursales = sucursalesFromJson(jsonString);

import 'dart:convert';

Sucursales sucursalesFromJson(String str) => Sucursales.fromJson(json.decode(str));

String sucursalesToJson(Sucursales data) => json.encode(data.toJson());

class Sucursales {
    int taxis_usuarios_direcciones_id;
    int taxisUsuariosDireccionesTaxisUsuariosId;
    String taxisUsuariosDirecciones;
    String taxisUsuariosDireccionesLong;
    String taxisUsuariosDireccionesLat;
    int taxisUsuariosDireccionesTaxisUsuariosDireccionesTiposId;

    Sucursales({
        this.taxis_usuarios_direcciones_id,
        this.taxisUsuariosDireccionesTaxisUsuariosId,
        this.taxisUsuariosDirecciones,
        this.taxisUsuariosDireccionesLong,
        this.taxisUsuariosDireccionesLat,
        this.taxisUsuariosDireccionesTaxisUsuariosDireccionesTiposId,
    });

    factory Sucursales.fromJson(Map<String, dynamic> json) => Sucursales(
        taxis_usuarios_direcciones_id: json["taxis_usuarios_direcciones_id"],
        taxisUsuariosDireccionesTaxisUsuariosId: json["taxis_usuarios_direcciones_taxis_usuarios_id"],
        taxisUsuariosDirecciones: json["taxis_usuarios_direcciones"],
        taxisUsuariosDireccionesLong: json["taxis_usuarios_direcciones_long"],
        taxisUsuariosDireccionesLat: json["taxis_usuarios_direcciones_lat"],
        taxisUsuariosDireccionesTaxisUsuariosDireccionesTiposId: json["taxis_usuarios_direcciones_taxis_usuarios_direcciones_tipos_id"],
    );

    Map<String, dynamic> toJson() => {
        "taxis_usuarios_direcciones_id": taxis_usuarios_direcciones_id,
        "taxis_usuarios_direcciones_taxis_usuarios_id": taxisUsuariosDireccionesTaxisUsuariosId,
        "taxis_usuarios_direcciones": taxisUsuariosDirecciones,
        "taxis_usuarios_direcciones_long": taxisUsuariosDireccionesLong,
        "taxis_usuarios_direcciones_lat": taxisUsuariosDireccionesLat,
        "taxis_usuarios_direcciones_taxis_usuarios_direcciones_tipos_id": taxisUsuariosDireccionesTaxisUsuariosDireccionesTiposId,
    };
}
