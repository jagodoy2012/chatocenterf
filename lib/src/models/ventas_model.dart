// To parse this JSON data, do
//
//     final ventasModel = ventasModelFromJson(jsonString);

import 'dart:convert';

VentasModel ventasModelFromJson(String str) => VentasModel.fromJson(json.decode(str));

String ventasModelToJson(VentasModel data) => json.encode(data.toJson());

class VentasModel {
  
    String idUsuario;
    String idDirecciones;
    String total;
    String noTransaccion;
    String estado;
    int idAsesor;
    String ventasTipoId;

    VentasModel({
      
        this.idUsuario = "1",
        this.idDirecciones = "1",
        this.total = "1",
        this.noTransaccion = "1",
        this.estado = "1",
        this.idAsesor = 1,
        this.ventasTipoId = "1",
    });

    factory VentasModel.fromJson(Map<String, dynamic> json) => VentasModel(
        
        idUsuario: json["id_usuario"],
        idDirecciones: json["id_direcciones"],
        total: json["total"],
        noTransaccion: json["no_transaccion"],
        estado: json["estado"],
        idAsesor: json["id_asesor"],
        ventasTipoId: json["ventas_tipo_id"],
    );

    Map<String, dynamic> toJson() => {
      
        "id_usuario": idUsuario,
        "id_direcciones": idDirecciones,
        "total": total,
        "no_transaccion": noTransaccion,
        "estado": estado,
        "id_asesor": idAsesor,
        "ventas_tipo_id": ventasTipoId,
    };
}
