// To parse this JSON data, do
//
//     final pedidosCliente = pedidosClienteFromJson(jsonString);

import 'dart:convert';

PedidosCliente pedidosClienteFromJson(String str) => PedidosCliente.fromJson(json.decode(str));

String pedidosClienteToJson(PedidosCliente data) => json.encode(data.toJson());

class PedidosCliente {
    PedidosCliente({
        this.nombre =  "",
        this.apellido =  "",
        this.taxisUsuariosPedirId = 0,
        this.taxisUsuariosPedirTaxisUsuariosId =  0,
        this.taxisUsuariosPedirLong =  "",
        this.taxisUsuariosPedirLat =  "",
        this.taxisUsuariosPedirDireccion =  "",
        this.taxisUsuariosPedirEstado =  "",
        this.taxisUsuariosPedirTaxisUsuariosDireccionesId =  0,
        this.taxisUsuariosPedirTaxisVehiculosTaxistaId =  "",
        this.taxisUsuariosPedirTaxisUsuariosPedirTipo =  "",
        this.taxisUsuariosPedirFecha,
        this.taxisUsuariosPedirHora,
        this.taxisUsuariosPedirTaxisUsuariosPedirPujaId =  "",
        this.taxisUsuariosPedirTaxisCobrosTipos =  "",
        this.taxisUsuariosPedirTaxisCategoriaId =  "",
        this.taxisMandadosId = 0,
        this.taxisMandadosTaxisUsuariosPedirId = 0,
        this.taxisMandadosImagen =  "",
        this.taxisMandadosPeso =  "",
        this.taxisMandadosDescripcion =  "",
    });

    String nombre;
    String apellido;
    int taxisUsuariosPedirId;
    int taxisUsuariosPedirTaxisUsuariosId;
    dynamic taxisUsuariosPedirLong;
    dynamic taxisUsuariosPedirLat;
    dynamic taxisUsuariosPedirDireccion;
    String taxisUsuariosPedirEstado;
    int taxisUsuariosPedirTaxisUsuariosDireccionesId;
    dynamic taxisUsuariosPedirTaxisVehiculosTaxistaId;
    dynamic taxisUsuariosPedirTaxisUsuariosPedirTipo;
    DateTime taxisUsuariosPedirFecha;
    String taxisUsuariosPedirHora;
    dynamic taxisUsuariosPedirTaxisUsuariosPedirPujaId;
    dynamic taxisUsuariosPedirTaxisCobrosTipos;
    dynamic taxisUsuariosPedirTaxisCategoriaId;
    int taxisMandadosId;
    int taxisMandadosTaxisUsuariosPedirId;
    dynamic taxisMandadosImagen;
    dynamic taxisMandadosPeso;
    String taxisMandadosDescripcion;

    factory PedidosCliente.fromJson(Map<String, dynamic> json) => PedidosCliente(
        nombre: json["nombre"],
        apellido: json["apellido"],
        taxisUsuariosPedirId: json["taxis_usuarios_pedir_id"],
        taxisUsuariosPedirTaxisUsuariosId: json["taxis_usuarios_pedir_taxis_usuarios_id"],
        taxisUsuariosPedirLong: json["taxis_usuarios_pedir_long"],
        taxisUsuariosPedirLat: json["taxis_usuarios_pedir_lat"],
        taxisUsuariosPedirDireccion: json["taxis_usuarios_pedir_direccion"],
        taxisUsuariosPedirEstado: json["taxis_usuarios_pedir_estado"],
        taxisUsuariosPedirTaxisUsuariosDireccionesId: json["taxis_usuarios_pedir_taxis_usuarios_direcciones_id"],
        taxisUsuariosPedirTaxisVehiculosTaxistaId: json["taxis_usuarios_pedir_taxis_vehiculos_taxista_id"],
        taxisUsuariosPedirTaxisUsuariosPedirTipo: json["taxis_usuarios_pedir_taxis_usuarios_pedir_tipo"],
        taxisUsuariosPedirFecha: DateTime.parse(json["taxis_usuarios_pedir_fecha"]),
        taxisUsuariosPedirHora: json["taxis_usuarios_pedir_hora"],
        taxisUsuariosPedirTaxisUsuariosPedirPujaId: json["taxis_usuarios_pedir_taxis_usuarios_pedir_puja_id"],
        taxisUsuariosPedirTaxisCobrosTipos: json["taxis_usuarios_pedir_taxis_cobros_tipos"],
        taxisUsuariosPedirTaxisCategoriaId: json["taxis_usuarios_pedir_taxis_categoria_id"],
        taxisMandadosId: json["taxis_mandados_id"],
        taxisMandadosTaxisUsuariosPedirId: json["taxis_mandados_taxis_usuarios_pedir_id"],
        taxisMandadosImagen: json["taxis_mandados_imagen"],
        taxisMandadosPeso: json["taxis_mandados_peso"],
        taxisMandadosDescripcion: json["taxis_mandados_descripcion"],
    );

    Map<String, dynamic> toJson() => {
        "nombre": nombre,
        "apellido": apellido,
        "taxis_usuarios_pedir_id": taxisUsuariosPedirId,
        "taxis_usuarios_pedir_taxis_usuarios_id": taxisUsuariosPedirTaxisUsuariosId,
        "taxis_usuarios_pedir_long": taxisUsuariosPedirLong,
        "taxis_usuarios_pedir_lat": taxisUsuariosPedirLat,
        "taxis_usuarios_pedir_direccion": taxisUsuariosPedirDireccion,
        "taxis_usuarios_pedir_estado": taxisUsuariosPedirEstado,
        "taxis_usuarios_pedir_taxis_usuarios_direcciones_id": taxisUsuariosPedirTaxisUsuariosDireccionesId,
        "taxis_usuarios_pedir_taxis_vehiculos_taxista_id": taxisUsuariosPedirTaxisVehiculosTaxistaId,
        "taxis_usuarios_pedir_taxis_usuarios_pedir_tipo": taxisUsuariosPedirTaxisUsuariosPedirTipo,
        "taxis_usuarios_pedir_fecha": taxisUsuariosPedirFecha.toIso8601String(),
        "taxis_usuarios_pedir_hora": taxisUsuariosPedirHora,
        "taxis_usuarios_pedir_taxis_usuarios_pedir_puja_id": taxisUsuariosPedirTaxisUsuariosPedirPujaId,
        "taxis_usuarios_pedir_taxis_cobros_tipos": taxisUsuariosPedirTaxisCobrosTipos,
        "taxis_usuarios_pedir_taxis_categoria_id": taxisUsuariosPedirTaxisCategoriaId,
        "taxis_mandados_id": taxisMandadosId,
        "taxis_mandados_taxis_usuarios_pedir_id": taxisMandadosTaxisUsuariosPedirId,
        "taxis_mandados_imagen": taxisMandadosImagen,
        "taxis_mandados_peso": taxisMandadosPeso,
        "taxis_mandados_descripcion": taxisMandadosDescripcion,
    };
}
