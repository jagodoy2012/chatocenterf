// To parse this JSON data, do
//
//     final ventasDetModel = ventasDetModelFromJson(jsonString);

import 'dart:convert';

VentasDetModel ventasDetModelFromJson(String str) => VentasDetModel.fromJson(json.decode(str));

String ventasDetModelToJson(VentasDetModel data) => json.encode(data.toJson());

class VentasDetModel {
    int id;
    int idVenta;
    int idProductos;
    int cantidad;
    double precio;
    String estado;
    String comentario;

    VentasDetModel({
        this.id,
        this.idVenta,
        this.idProductos,
        this.cantidad,
        this.precio,
        this.estado,
        this.comentario,
    });

    factory VentasDetModel.fromJson(Map<String, dynamic> json) => VentasDetModel(
        id: json["id"],
        idVenta: json["id_venta"],
        idProductos: json["id_productos"],
        cantidad: json["cantidad"],
        precio: json["precio"].toDouble(),
        estado: json["estado"],
        comentario: json["comentario"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "id_venta": idVenta,
        "id_productos": idProductos,
        "cantidad": cantidad,
        "precio": precio,
        "estado": estado,
        "comentario": comentario,
    };
}
