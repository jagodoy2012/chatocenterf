// To parse this JSON data, do
//
//     final usuariosNPassModel = usuariosNPassModelFromJson(jsonString);

import 'dart:convert';

UsuariosNPassModel usuariosNPassModelFromJson(String str) => UsuariosNPassModel.fromJson(json.decode(str));

String usuariosNPassModelToJson(UsuariosNPassModel data) => json.encode(data.toJson());

class UsuariosNPassModel {
    int id;
    String nombre;
    String apellido;
    String correo;
    String email;
    String celular;
    String telefono;
    int idUsuariosTipos;
    String fb;
    String inst;
    String ubicacion;
    String descripcion;
    String estado;

    UsuariosNPassModel({
        this.id,
        this.nombre  = "",
        this.apellido = "",
        this.correo = "",
        this.email = "",
        this.celular = "",
        this.telefono = "",
        this.idUsuariosTipos = 1,
        this.fb = "",
        this.inst = "",
        this.ubicacion = "",
        this.descripcion = "",
        this.estado = "",
    });

    factory UsuariosNPassModel.fromJson(Map<String, dynamic> json) => UsuariosNPassModel(
        id: json["id"],
        nombre: json["nombre"],
        apellido: json["apellido"],
        correo: json["correo"],
        email: json["email"],
        celular: json["celular"],
        telefono: json["telefono"],
        idUsuariosTipos: json["id_usuarios_tipos"],
        fb: json["fb"],
        inst: json["inst"],
        ubicacion: json["ubicacion"],
        descripcion: json["descripcion"],
        estado: json["estado"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "nombre": nombre,
        "apellido": apellido,
        "correo": correo,
        "email": email,
        "celular": celular,
        "telefono": telefono,
        "id_usuarios_tipos": idUsuariosTipos,
        "fb": fb,
        "inst": inst,
        "ubicacion": ubicacion,
        "descripcion": descripcion,
        "estado": estado,
    };
}
