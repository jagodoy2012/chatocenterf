// To parse this JSON data, do
//
//     final mandadosModel = mandadosModelFromJson(jsonString);

import 'dart:convert';

MandadosModel mandadosModelFromJson(String str) => MandadosModel.fromJson(json.decode(str));

String mandadosModelToJson(MandadosModel data) => json.encode(data.toJson());

class MandadosModel {
    int taxis_usuarios_pedir_taxis_usuarios_id;
    String taxis_usuarios_pedir_direccion;
    String taxisUsuariosPedirEstado;
    int taxisUsuariosPedirTaxisUsuariosDireccionesId;
    String taxisMandadosDescripcion;
    

    MandadosModel({
        this.taxis_usuarios_pedir_direccion,
        this.taxis_usuarios_pedir_taxis_usuarios_id,
        this.taxisUsuariosPedirEstado,
        this.taxisUsuariosPedirTaxisUsuariosDireccionesId,
        this.taxisMandadosDescripcion,
    });

    factory MandadosModel.fromJson(Map<String, dynamic> json) => MandadosModel(
        taxis_usuarios_pedir_taxis_usuarios_id: json["taxis_usuarios_pedir_taxis_usuarios_id"],
        taxisUsuariosPedirEstado: json["taxis_usuarios_pedir_estado"],
        taxisUsuariosPedirTaxisUsuariosDireccionesId: json["taxis_usuarios_pedir_taxis_usuarios_direcciones_id"],
        taxisMandadosDescripcion: json["taxis_mandados_descripcion"],
        taxis_usuarios_pedir_direccion: json["taxis_usuarios_pedir_direccion"],
    );

    Map<String, dynamic> toJson() => {
        "taxis_usuarios_pedir_taxis_usuarios_id": taxis_usuarios_pedir_taxis_usuarios_id,
        "taxis_usuarios_pedir_estado": taxisUsuariosPedirEstado,
        "taxis_usuarios_pedir_taxis_usuarios_direcciones_id": taxisUsuariosPedirTaxisUsuariosDireccionesId,
        "taxis_mandados_descripcion": taxisMandadosDescripcion,
        "taxis_usuarios_pedir_direccion": taxis_usuarios_pedir_direccion,
    };
}
