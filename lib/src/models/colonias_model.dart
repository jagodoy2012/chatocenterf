// To parse this JSON data, do
//
//     final coloniasModel = coloniasModelFromJson(jsonString);

import 'dart:convert';

ColoniasModel coloniasModelFromJson(String str) => ColoniasModel.fromJson(json.decode(str));

String coloniasModelToJson(ColoniasModel data) => json.encode(data.toJson());

class ColoniasModel {
    ColoniasModel({
        this.id ,
        this.nombre = "",
        this.descripcion = "",
        this.idZona  = 0,
        this.estado = 1,
        this.idSucursal = 0,
    });
 
    int id;
    String nombre;
    String descripcion;
    int idZona;
    int estado;
    int idSucursal;

    factory ColoniasModel.fromJson(Map<String, dynamic> json) => ColoniasModel(
        id: json["id"],
        nombre: json["nombre"],
        descripcion: json["descripcion"],
        idZona: json["id_zona"],
        estado: json["estado"],
        idSucursal: json["id_sucursal"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "nombre": nombre,
        "descripcion": descripcion,
        "id_zona": idZona,
        "estado": estado,
        "id_sucursal": idSucursal,
    };
}
