// To parse this JSON data, do
//
//     final fact = factFromJson(jsonString);

import 'dart:convert';

Fact factFromJson(String str) => Fact.fromJson(json.decode(str));

String factToJson(Fact data) => json.encode(data.toJson());

class Fact {
    String idusuario;
    String iddirecc;
    String direcc;
    String nombre;
    String nit;
    String tel;
    String envio;
    double total;

    Fact({
        this.idusuario,
        this.iddirecc,
        this.direcc,
        this.nombre,
        this.nit,
        this.tel,
        this.envio = "",
        this.total,
    });

    factory Fact.fromJson(Map<String, dynamic> json) => Fact(
        idusuario: json["idusuario"],
        iddirecc: json["iddirecc"],
        direcc: json["direcc"],
        nombre: json["nombre"],
        nit: json["nit"],
        tel: json["tel"],
        envio: json["envio"],
        total: json["total"],
    );

    Map<String, dynamic> toJson() => {
        "idusuario": idusuario,
        "iddirecc": iddirecc,
        "direcc": direcc,
        "nombre": nombre,
        "nit": nit,
        "tel": tel,
        "envio": envio,
        "total": total,
    };
}
