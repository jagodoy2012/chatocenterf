 
import 'package:chatocenter/src/bloc/pedidos_bloc.dart';
import 'package:chatocenter/src/bloc/provider.dart';
import 'package:chatocenter/src/bloc/usuarios_bloc.dart';
import 'package:chatocenter/src/models/pedidos_model.dart';
import 'package:chatocenter/src/models/productos_model.dart';
import 'package:chatocenter/src/models/usuarios_model.dart';
import 'package:chatocenter/src/pages/direcciones_page.dart';
import 'package:chatocenter/src/pages/usuario_page.dart';
import 'package:chatocenter/src/preferencias_usuario/preferencias_usuario.dart';
import 'package:chatocenter/src/providers/usuarios_provider.dart';
import 'package:flutter/material.dart';






class PedidosPage extends StatefulWidget {
//final categoriasModel = CategoriasProvider();
 //final categoriasProvider = new CategoriasProvider();
  @override
  _PedidosPageState createState() => _PedidosPageState();
}

class _PedidosPageState extends State<PedidosPage> {
void initState() {
    super.initState();
   
  }
  final usuar = new UsuariosProvider();
   int tel = 0;
     String estado = "";
  @override  
    
  Widget build(BuildContext context) {
final ProductosModel prodData = ModalRoute.of(context).settings.arguments;  
 if(prodData!=null){
      
       tel = int.parse(prodData.titulo);
    }


    
    final pedidosBloc = Provider.pedidosBloc(context);
    pedidosBloc.cargarTelUsuarios(tel);


       return Scaffold(
           appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text('PEDIDOS'),
       
      ), 
      body:  
      
             
                    _crearListado(pedidosBloc),
                
                    
            
      
      //floatingActionButton: _crearBoton(context),
      
 
       );
  }

  Widget _crearListado( PedidosBloc pedidosBloc){ 


    
        return  
        RefreshIndicator(
                onRefresh:  (){

                return  pedidosBloc.cargarTelUsuarios(tel);
                },
          child: StreamBuilder(
        stream: pedidosBloc.pedidosStream,
        
        builder: (BuildContext context, AsyncSnapshot<List<PedidosCliente>> snapshot){
          final pedido  = snapshot.data;
          if(snapshot.hasData){
            return  
            
            ListView.builder(
            itemCount: pedido.length,
            itemBuilder: (context, i) => _crearItem(context, pedido[i], pedidosBloc),
            );
          } 
          else{
            return Center(child: CircularProgressIndicator(),);
          }
          
        },
      ),
    );

   

  }

Widget _crearItem(BuildContext context, PedidosCliente pedidos, PedidosBloc pedidosBloc){
   
   if( pedidos.taxisUsuariosPedirEstado == "1"){
         estado = "SIN PROCESAR"; 
   }
  else{
      if( pedidos.taxisUsuariosPedirEstado == "2"){
         estado = "EN PREPARACION"; 
   }
    else{
       if( pedidos.taxisUsuariosPedirEstado == "3"){
         estado = "ENVIADO"; 
   }
    }
  }

  return Dismissible(
      key: UniqueKey(),
      background: Container(
        color: Colors.red,
        child: Icon(Icons.delete,color: Colors.white,size: 50.0,),
        alignment: Alignment.centerLeft,
      ),
       secondaryBackground: Container(
        child: Icon(Icons.thumb_up, color: Colors.white, size: 50.0,),
        color: Colors.lightGreenAccent,
        alignment: Alignment.centerRight,
      ),
      onDismissed: (DismissDirection dir){
        // Remove the dismissed item from the list
  
    
    
   
  setState(() {
      // Navigator.push(context, MaterialPageRoute(builder: (context) => UsuariosAsigCliePage(), settings: RouteSettings(arguments: usuarios))).then((value) {
  
 
     // return true;
     
      // refresh state
    
   //});
  });
    
    
        //setState(()=> usuariosBloc.borrarUsuarios(usuarios));
        //categoriasProvider.borrarCategoria(categorias);
       // usuariosBloc.borrarUsuarios(usuarios);
      
      },
     
      child: Card( 
        child:  Column(
          children: <Widget>[ 
          
           
            ListTile(
     leading: Icon(Icons.location_city), 
      title:  Text('Nombre: ${pedidos.nombre}'' ''${pedidos.apellido}'),
      subtitle: Text('Pedido No.: ${pedidos.taxisUsuariosPedirId}  Estado: $estado '),
      onTap: () {
    

     }  
    ),
  
          ] 
        ),
      ) 


  );
 
}

  

   
}