

import 'dart:io';


import 'package:chatocenter/src/bloc/productos_bloc.dart';
import 'package:chatocenter/src/bloc/provider.dart';
import 'package:chatocenter/src/models/productos_model.dart';
import 'package:chatocenter/src/pages/buscarpedido_page.dart';
import 'package:chatocenter/src/pages/usuarios_page.dart';
import 'package:chatocenter/src/preferencias_usuario/preferencias_usuario.dart';
import 'package:flutter/material.dart';




import 'package:chatocenter/src/utils/utils.dart' as utils;




import 'package:image_picker/image_picker.dart';



class NumeroPage extends StatefulWidget {
  
  @override
 
  _NumeroPageState createState() => _NumeroPageState();
}

class _NumeroPageState extends State<NumeroPage> {
  final formKey = GlobalKey<FormState>();
  final scaffoldKey= GlobalKey<ScaffoldState>();
     final _prefs = new PreferenciasUsuario(); // guardar en storage del celular 
 
  ProductosBloc productoBloc;  //bloc implementar 
  ProductosModel productos = new ProductosModel();
  bool _guardando = false; 
  File foto;
  int boton = 0;
  @override
  Widget build(BuildContext context) {

    productoBloc = Provider.productoBloc(context);
   final ProductosModel prodData = ModalRoute.of(context).settings.arguments;  //verificar si vienen argumentos 
    if(prodData!=null){
      productos = prodData;
       boton = 1;
    }


    return Scaffold(
      key: scaffoldKey,
      
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text('NUMERO DE TELEFONO'),
       actions: <Widget>[
          
          IconButton(
            
            icon: Icon(Icons.youtube_searched_for),
             onPressed:(){
                
            Navigator.push(context, MaterialPageRoute(builder: (context) => BuscarpedidoPage())).then((value) {
  setState(() {
          
      // refresh state
    });
  });
            
              }, color: Colors.lightGreenAccent, iconSize: 40,),
            
            
        ]
      ),
      body: SingleChildScrollView(
       child: Container(
         padding: EdgeInsets.all(15.0),
         child: Form(
           key: formKey, // identificador unico de este form 
           child: Column(
              children: <Widget>[
                  _mostrarFoto(),
                  _crearNombre(),
                 

                  _crearBoton()
                  
                
                 
          
              ]
             
           ))
       )
      ),
     
      
    );
  }

Widget _crearNombre()
{
return TextFormField(
  initialValue: productos.titulo, //valor inicial 
  textCapitalization: TextCapitalization.sentences,
  decoration: InputDecoration( 
    labelText: 'Numero de telefono'
  ),
  onSaved: (value) => productos.titulo = value,
  validator: (value){
    if(value.length < 8 ){
      return 'Ingrese Numero';
    }else{
      return null;
    }
  },
);
}



 




Widget  _crearBoton(){
  return RaisedButton.icon(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
    color: Colors.black,
    textColor: Colors.white,
    onPressed:(_guardando)? null:_submit, icon: Icon(Icons.save), label: Text('BUSCAR')

    );
}

  

void _submit ()async{ // para disparar los mensajes 


if(!formKey.currentState.validate())return; //si es invalido manda error regresa true si es valido el formulario o false si no es valido

  formKey.currentState.save();
    // para bloquear boton de guardar 

  setState(() {
    //para redibujar widget 
  });
   

    
  mostrarSnackbar('Buscado');
   _prefs.returnpag = 1;
  
Navigator.push(context, MaterialPageRoute(builder: (context) => UsuariosPage(), settings: RouteSettings(arguments: productos))).then((value) {
  setState(() {
          
      // refresh state
    });
  });
 

}



void mostrarSnackbar(String mensaje){
/// mostrar mensaje al guardar 
  final snackbar = SnackBar(

    content: Text(mensaje),
    duration: Duration(milliseconds: 1500),
  );

  scaffoldKey.currentState.showSnackBar(snackbar);

} 

Widget _mostrarFoto(){
  
  if(productos.img != "" && productos.img != " "){
    
    return FadeInImage(
      placeholder: AssetImage("assets/original.gif"), 
      image:   NetworkImage(productos.img),
      height: 300.0,
      fit: BoxFit.contain,
      );
  }
  else{
    return Image(
      image: AssetImage(foto?.path ??'assets/almacarone.jpg'), /// si tiene un valor lo pone si no pone la de default
      height: 300.00,
      fit: BoxFit.cover,
    );
  }
}

_seleccionarFoto()async{
 foto = await ImagePicker.pickImage(
    source: ImageSource.gallery
    );

    if(foto != " "){
        productos.img = " ";
    }
    setState(() {
      
    });
}


}