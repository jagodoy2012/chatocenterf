
import 'package:chatocenter/src/models/fact_model.dart';
import 'package:chatocenter/src/models/ventasdb_model.dart';
import 'package:chatocenter/src/pages/producto_page.dart';
import 'package:chatocenter/src/pages/sucursales_page.dart';
import 'package:chatocenter/src/preferencias_usuario/preferencias_usuario.dart';
import 'package:chatocenter/src/providers/db_provider.dart';
import 'package:chatocenter/src/providers/mandados_provider.dart';
import 'package:chatocenter/src/providers/ventas_provider.dart';
import 'package:flutter/material.dart';
 




class CarritoPage extends StatefulWidget {
//final categoriasModel = CategoriasProvider();
 //final categoriasProvider = new CategoriasProvider();
  @override
  _CarritoPageState createState() => _CarritoPageState();
} 

class _CarritoPageState extends State<CarritoPage> {
void initState() {
    super.initState();
   
  }
  String envio = ""; 
  double suma = 0.00 ; 
  double subtota= 0.00;
  int encab = 0;
  Fact datosfactss = Fact();
  bool _guardando = false;
  @override 
  
  Widget build(BuildContext context) { 

   final Fact prodData = ModalRoute.of(context).settings.arguments;  //verificar si vienen argumentos 
    if(prodData!=null){
     datosfactss.nombre = prodData.nombre;
     datosfactss.idusuario = prodData.idusuario;
     datosfactss.direcc = prodData.direcc;
     datosfactss.iddirecc = prodData.iddirecc;
     datosfactss.nit = prodData.nit;
     datosfactss.tel = prodData.tel;
    
    }
 
 

       return Scaffold(
            appBar: PreferredSize(
              preferredSize: Size.fromHeight(80.0), 
                          child: AppBar(
               
           flexibleSpace: Container(),

        backgroundColor: Colors.black,
        title: 
               Text('Nombre: '+datosfactss.nombre+' Tel: '+datosfactss.tel+'\nDir: '+datosfactss.direcc+'\nNit: '+datosfactss.nit, style: TextStyle(fontSize: 15),),
         
        
       
      ),
            ),
      body: 
                _crearListado(),
          
      floatingActionButton: _crearBoton(context,  datosfactss),
      
        
       
    );
  }
Widget _crearNombre()
{
return TextFormField(
  initialValue: datosfactss.nombre, //valor inicial 
  textCapitalization: TextCapitalization.sentences,
  decoration: InputDecoration( 
    labelText: 'Nombre'
  ),


);
}
  Widget _crearListado( ){ 
    _crearNombre();
   
    envio ="Nombre: "+ datosfactss.nombre+"\nDireccion: "+ datosfactss.direcc+"\nTelefono: "+datosfactss.tel+"\nNit: "+datosfactss.nit+"\nVENTA:";
    return FutureBuilder<List<Ventasdb>>(
        future: DBProvider.db.getTodasVentas(),
        builder: (BuildContext context, AsyncSnapshot<List<Ventasdb>> snapshot){
          final producto  = snapshot.data; 
          suma = 0.00;
          if(snapshot.hasData){
            return  
         
            ListView.builder(
            itemCount: producto.length,
           
            itemBuilder: (context, i) => _crearItem(context, producto[i]),
            );
          } 
          else{
            return Center(child: CircularProgressIndicator(),);
          }
          
        },
     
    
);
   
  
  }

Widget _crearItem(BuildContext context, Ventasdb productos){
  print(productos.producto);

  envio = envio +"\n"+"Producto:" +  "\n" + productos.producto +"\n"+"Comentarios:" + productos.comentario +  "\n" +  "Cantidad:" +  "\n" +  productos.cantidad.toString() + "\nPrecio: "+productos.precio.toString();
  datosfactss.envio = envio;
  suma = suma + (productos.cantidad * productos.precio);
  datosfactss.total = suma;
  encab = encab+1;
  subtota = (productos.cantidad * productos.precio);
  return Dismissible(
      key: UniqueKey(),
      background: Container(
        color: Colors.red,
      ), 
      onDismissed: (direccion){
 setState(() {
  
      suma = 0.00;
      int id1 = productos.id;
        DBProvider.db.deleteVentas(id1);
      

    });
    

        suma = double.parse(suma.toStringAsFixed(2));
        
      },
      child: Card(  
        child:  Column(
          children: <Widget>[
         if(encab==1) 
               new ListTile(
      title:  
      
      Text('\nPedido'+' ${productos.producto}\nComentario:  ${productos.comentario} \nPRECIO: Q${productos.precio}\nCantidad: ${productos.cantidad}\nSUB-TOTAL: ${subtota} --- TOTAL: ${suma} '),
     
      onTap: () => 
      Navigator.push(context, MaterialPageRoute(builder: (context) => ProductoPage(), settings: RouteSettings(arguments: productos))).then((value) {
  setState(() {
  final prefs = new PreferenciasUsuario();
   
          if (prefs.returnpag == 0) {
             Navigator.pop(context);
          }
      // refresh state
    });
  }),

    )
         
      else    new ListTile(
        
      title:  
      
      Text('\nPedido: '+'${productos.producto}\nComentario:${productos.comentario}PRECIO: Q${productos.precio}\nCantidad: ${productos.cantidad}\nSUB-TOTAL: ${subtota} --- TOTAL: ${suma} '),
      
      onTap: () {
      
     
   
   
      }

       
    )   
                     
           
           
         
          ]
        ),
      ) 


  );

}

  _crearBoton(BuildContext context,  datosfact){
 

 
    return SizedBox(
      width: 80.0,
      height: 80.0,
      
          child: FloatingActionButton(
            
        backgroundColor: Colors.black,
        
        child: Icon(Icons.check_circle, color: Colors.lightGreenAccent, size: 40.0,),
        onPressed: _guardando ? null : () async { 

  setState(() {
      _guardando = true;
    });
           if( suma > 67.0){
             
    final prefs = new PreferenciasUsuario();
            mandados.taxisMandadosDescripcion = envio;
            mandados.taxisUsuariosPedirEstado = "1";
            mandados.taxisUsuariosPedirTaxisUsuariosDireccionesId = prefs.idsucursal;
      
      print("pruebas de insert en tabla");
      final _prefs = new PreferenciasUsuario();
      ventas.idUsuario = datosfactss.idusuario;
      ventas.idDirecciones = datosfactss.iddirecc;
      ventas.total = datosfactss.total.toString();
      ventas.idAsesor =  prefs.idvendedor;
 
      _prefs.returnpag = 0 ;
       
   
    

   int decodedData = await VentasProvider().crearVentas(ventas); 
    mandados.taxis_usuarios_pedir_taxis_usuarios_id = decodedData;
 
  
    MandadosProvider().crearMandad(mandados);
  
    DBProvider.db.deleteallVentas();
             Navigator.pop(context);
           }
     }
        ),
    );
     

 } // fin del if

   

   
}