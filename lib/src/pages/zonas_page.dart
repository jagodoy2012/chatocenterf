

import 'package:chatocenter/src/bloc/provider.dart';
import 'package:chatocenter/src/bloc/zonas_bloc.dart';
import 'package:chatocenter/src/models/munis_model.dart';
import 'package:chatocenter/src/models/zonas_model.dart';
import 'package:chatocenter/src/pages/colonias_page.dart';

import 'package:chatocenter/src/preferencias_usuario/preferencias_usuario.dart';
import 'package:flutter/material.dart';
 







class ZonasPage extends StatefulWidget {
//final categoriasModel = CategoriasProvider();
 //final categoriasProvider = new CategoriasProvider();

  @override
  _ZonasPageState createState() => _ZonasPageState();
} 
 ZonasModel zonas = new ZonasModel();
  int idusuario = 0;
  dynamic zona = "";
class _ZonasPageState extends State<ZonasPage> {
void initState() {
    super.initState();
   
  }
  @override 
  
  Widget build(BuildContext context) { 

   final zonasBloc = Provider.zonasBloc(context);
    
  final MunisModel prodData = ModalRoute.of(context).settings.arguments;  //verificar si vienen argumentos 
    if(prodData!=null){
      
      idusuario = prodData.id;
   
    
    }
 
  cargarzonasid();
  


       return Scaffold(
         appBar: AppBar(
       
        backgroundColor: Colors.black,
        title: Text('ZONAS'),
        
        ),
      body:  _crearListado(zonasBloc),
      
      
    );
  }

  Widget _crearListado( ZonasBloc zonasBloc){ 

   
      
  
    return  
    RefreshIndicator(
            onRefresh:  (){

                return  zonasBloc.cargarZonasid(idusuario.toString());
                }, 
                 
          child: 
          
          StreamBuilder(

        stream: zonasBloc.zonasStream,
        
         
        builder: (BuildContext context, AsyncSnapshot<List<ZonasModel>> snapshot){
            zona  = snapshot.data;
          if(snapshot.hasData){
            return  
            
            ListView.builder(
            itemCount: zona.length,
            itemBuilder: (context, i) => _crearItem(context, zona[i], zonasBloc),
            );
          } 
          else{
            return Center(child: CircularProgressIndicator(),);
          }
          
        },
      ),
    );

  
 
  }
 
  cargarzonasid() async {
    final zonasBloc = Provider.zonasBloc(context);
  await zonasBloc.cargarZonasid(idusuario.toString());
  }
Widget _crearItem(BuildContext context, ZonasModel zonas, ZonasBloc zonasBloc){

  return Dismissible(
      key: UniqueKey(),
      background: Container(
        color: Colors.red,
      ),
     
      child: Card( 
        child:  Column(
          children: <Widget>[  
            
               new ListTile(
                 leading: Icon(Icons.location_city),
      title:  Text('Zonas: ${zonas.nombre}'),
   
      onTap: () async {
 
          await cargarcoloniasid(zonas.id);
      Navigator.push(context, MaterialPageRoute(builder: (context) => ColoniasPage(), settings: RouteSettings(arguments: zonas))).then((value) {
  setState(() {
   
       // refresh state 
          final _prefs = new PreferenciasUsuario();
            if(_prefs.returndir == 0)
            {
                  Navigator.pop(context);
            }
    });
  }); 
       }
    )
             
  
           
         
          ]
        ),
      ) 


  );

} 


  cargarcoloniasid(int idcolonias) async {
    final coloniasBloc = Provider.coloniasBloc(context);
  await coloniasBloc.cargarColoniasid(idcolonias.toString());
  }
   
}