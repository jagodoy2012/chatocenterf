

import 'package:chatocenter/src/bloc/productos_bloc.dart';
import 'package:chatocenter/src/bloc/provider.dart';
import 'package:chatocenter/src/models/fact_model.dart';
import 'package:chatocenter/src/models/productos_model.dart';
import 'package:chatocenter/src/pages/carrito_page.dart';
import 'package:chatocenter/src/pages/producto_page.dart';
import 'package:chatocenter/src/preferencias_usuario/preferencias_usuario.dart';
import 'package:flutter/material.dart';

import 'package:flutter_speed_dial/flutter_speed_dial.dart';
 


class ProductosPage extends StatefulWidget {
//final categoriasModel = CategoriasProvider();
 //final categoriasProvider = new CategoriasProvider();
  @override
  _ProductosPageState createState() => _ProductosPageState();
}  

class _ProductosPageState extends State<ProductosPage> {
void initState() {
    super.initState();
   
  }
 
  Fact datosfact = Fact();
  @override 
    
  Widget build(BuildContext context) { 

    final productosBloc = Provider.productoBloc(context);
    productosBloc.cargarProductos();
final Fact prodData = ModalRoute.of(context).settings.arguments;  //verificar si vienen argumentos 
    if(prodData!=null){
     datosfact.nombre = prodData.nombre;
     datosfact.idusuario = prodData.idusuario;
     datosfact.direcc = prodData.direcc;
     datosfact.iddirecc = prodData.iddirecc;
     datosfact.nit = prodData.nit;
     datosfact.tel =prodData.tel;
   
    }

       return Scaffold(
      body:  _crearListado(productosBloc),
     floatingActionButton:   _crearBoton(context),
      
    );
  } 

  Widget _crearListado( ProductosBloc productosBloc){ 


    return  
    RefreshIndicator(
            onRefresh: productosBloc.cargarProductos,
          child: StreamBuilder(
        stream: productosBloc.productosStream,
        
        builder: (BuildContext context, AsyncSnapshot<List<ProductosModel>> snapshot){
          final producto  = snapshot.data;
          if(snapshot.hasData){
            return  
            
            ListView.builder(
            itemCount: producto.length,
            itemBuilder: (context, i) => _crearItem(context, producto[i], productosBloc),
            );
          } 
          else{
            return Center(child: CircularProgressIndicator(),);
          }
          
        },
      ),
    );

   

  }

Widget _crearItem(BuildContext context, ProductosModel productos, ProductosBloc productosBloc){

  return Dismissible(
      key: UniqueKey(),
      background: Container(
        color: Colors.red,
      ),
      onDismissed: (direccion){
        //categoriasProvider.borrarCategoria(categorias);
        productosBloc.borrarProductos(productos);
          
      },
      child: Card( 
        child:  Column(
          children: <Widget>[
            (productos.img == " ")
            ? Image(image: AssetImage('assets/foragrop.png'))
            : FadeInImage(
              image: NetworkImage(productos.img),
              placeholder: AssetImage('assets/original.gif'),
              height: 300.0,
              width: double.infinity,
              fit:  BoxFit.cover,
            ),
            ExpansionTile(title: Text('${productos.titulo}'),
             children: <Widget>[
               new ListTile(
      title:  Text('PRECIO: Q${productos.precio}'),
      subtitle: Text('${productos.descripcion}'),
      onTap: () => 
      Navigator.push(context, MaterialPageRoute(builder: (context) => ProductoPage(), settings: RouteSettings(arguments: productos))).then((value) {
  setState(() {
  
      // refresh state
    });
  }),
       
    )
             
            ]
            ),
           
         
          ]
        ),
      ) 


  );

}

  _crearBoton(BuildContext context){
    return SizedBox(
      width: 80.0,
      height: 80.0,
          child: FloatingActionButton(
        
        backgroundColor: Colors.black, 
         
        child: Icon(Icons.shopping_cart, color: Colors.lightGreenAccent, size: 40.0,),
        onPressed: ()=> Navigator.push(context, MaterialPageRoute(builder: (context) => CarritoPage(), settings: RouteSettings(arguments: datosfact))).then((value) {
  setState(() {
        // refresh state
         final prefs = new PreferenciasUsuario();
    
          if (prefs.returnpag == 0) {
             Navigator.pop(context);
          }
         
      }); 
  }),
        ),
    );
    }

   
}