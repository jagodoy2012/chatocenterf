


import 'package:chatocenter/src/bloc/colonias_bloc.dart';
import 'package:chatocenter/src/bloc/provider.dart';
import 'package:chatocenter/src/models/colonias_model.dart';
import 'package:chatocenter/src/models/zonas_model.dart';
import 'package:chatocenter/src/pages/direccion_page.dart';
import 'package:chatocenter/src/pages/direcciones_page.dart';
import 'package:chatocenter/src/preferencias_usuario/preferencias_usuario.dart';
import 'package:flutter/material.dart';
 







class ColoniasPage extends StatefulWidget {
//final categoriasModel = CategoriasProvider();
 //final categoriasProvider = new CategoriasProvider();

  @override
  _ColoniasPageState createState() => _ColoniasPageState();
} 
 ColoniasModel colonias = new ColoniasModel();
  int idusuario = 0;
  dynamic colonia = "";
class _ColoniasPageState extends State<ColoniasPage> {
void initState() {
    super.initState();
   
  }
  @override 
  
  Widget build(BuildContext context) { 

   final coloniasBloc = Provider.coloniasBloc(context);
    
  final ZonasModel prodData = ModalRoute.of(context).settings.arguments;  //verificar si vienen argumentos 
    if(prodData!=null){
      
      idusuario = prodData.id;
   
    
    }
 
  cargarColoniasid();
  


       return Scaffold(
         appBar: AppBar(
       
        backgroundColor: Colors.black,
        title: Text('COLONIAS'),
        
        ),
      body:  _crearListado(coloniasBloc),
     
      
    );
  }

  Widget _crearListado( ColoniasBloc coloniasBloc){ 

   
      
  
    return  
    RefreshIndicator(
            onRefresh:  (){

                return  coloniasBloc.cargarColoniasid(idusuario.toString());
                }, 
                 
          child: 
          
          StreamBuilder(

        stream: coloniasBloc.coloniasStream,
        
         
        builder: (BuildContext context, AsyncSnapshot<List<ColoniasModel>> snapshot){
            colonia  = snapshot.data;
          if(snapshot.hasData){
            return  
            
            ListView.builder(
            itemCount: colonia.length,
            itemBuilder: (context, i) => _crearItem(context, colonia[i], coloniasBloc),
            );
          } 
          else{
            return Center(child: CircularProgressIndicator(),);
          }
          
        },
      ),
    );

  
 
  }
 
 
Widget _crearItem(BuildContext context, ColoniasModel colonias, ColoniasBloc coloniasBloc){
 
  return Dismissible(
      key: UniqueKey(),
      background: Container(
        color: Colors.red,
      ),
      onDismissed: (zon){
        //categoriasProvider.borrarCategoria(categorias);
        coloniasBloc.borrarColonias(colonias);
          
      },
      child: Card( 
        child:  Column(
          children: <Widget>[  
            
               new ListTile(
                 leading: Icon(Icons.location_city),
      title:  Text('Colonia: ${colonias.nombre}'),  
   
      onTap: () {

 Navigator.push(context, MaterialPageRoute(builder: (context) => DireccioPage(), settings: RouteSettings(arguments: colonias))).then((value) {
  setState(() {
   
       // refresh state 
       final _prefs = new PreferenciasUsuario();
            if(_prefs.returndir == 0)
            {
                  Navigator.pop(context);
            }
          
    });

 });

       }
    )
             
  
           
         
          ]
        ),
      ) 


  );

} 



    cargarColoniasid() async {
    final coloniasBloc = Provider.coloniasBloc(context);
  await coloniasBloc.cargarColoniasid(idusuario.toString());
  }
}