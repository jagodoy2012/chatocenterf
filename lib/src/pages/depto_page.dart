 


import 'package:chatocenter/src/bloc/depto_bloc.dart';
import 'package:chatocenter/src/bloc/provider.dart';
import 'package:chatocenter/src/models/depto_model.dart';
import 'package:chatocenter/src/pages/munis_page.dart';
import 'package:chatocenter/src/preferencias_usuario/preferencias_usuario.dart';
import 'package:flutter/material.dart';
 







class DeptoPage extends StatefulWidget {
//final categoriasModel = CategoriasProvider();
 //final categoriasProvider = new CategoriasProvider();

  @override
  _DeptoPageState createState() => _DeptoPageState();
}
 DeptoModel depto = new DeptoModel();
  int idusuario = 0;
  String nombr = "";
  String nit = "";
  String tel = "";
class _DeptoPageState extends State<DeptoPage> {
void initState() {
    super.initState();
   
  }
  @override 
  
  Widget build(BuildContext context) { 

    final deptoBloc = Provider.deptoBloc(context);
    
  final DeptoModel prodData = ModalRoute.of(context).settings.arguments;  //verificar si vienen argumentos 
    

deptoBloc.cargarDepto();
       return Scaffold(
         appBar: AppBar(
       
        backgroundColor: Colors.black,
        title: Text('Departamentos'),
        
        ),
      body:  _crearListado(deptoBloc),
      
      
    );
  }

  Widget _crearListado( DeptoBloc deptoBloc){ 


    return  
    RefreshIndicator(
            onRefresh:  (){

                return  deptoBloc.cargarDepto();
                }, 
                 
          child: StreamBuilder(
        stream: deptoBloc.deptoStream,
        
         
        builder: (BuildContext context, AsyncSnapshot<List<DeptoModel>> snapshot){
          final deptos  = snapshot.data;
          if(snapshot.hasData){
            return  
            
            ListView.builder(
            itemCount: deptos.length,
            itemBuilder: (context, i) => _crearItem(context, deptos[i], deptoBloc),
            );
          } 
          else{
            return Center(child: CircularProgressIndicator(),);
          }
          
        },
      ),
    );

   

  }

Widget _crearItem(BuildContext context, DeptoModel deptos, DeptoBloc deptosBloc){

  return Dismissible(
      key: UniqueKey(),
      background: Container(
        color: Colors.red,
      ),
     
      child: Card( 
        child:  Column( 
          children: <Widget>[  
            
               new ListTile(
                 leading: Icon(Icons.location_city),
      title:  Text('Direccion: ${deptos.nombre}'),
   
      onTap: () async {
 
           // datosfac.direcc  = direcciones.direccion;
            //datosfac.iddirecc = direcciones.id.toString();
           // datosfac.nombre = nombr;
           // datosfac.nit = nit; 
           // datosfac.idusuario = idusuario.toString();
          //  datosfac.tel = tel;
         await cargarmunisid(deptos.id);
      Navigator.push(context, MaterialPageRoute(builder: (context) => MunisPage(), settings: RouteSettings(arguments: deptos))).then((value) {
  setState(() {
  
       // refresh state 
      final _prefs = new PreferenciasUsuario();
            if(_prefs.returndir == 0)
            {
                  Navigator.pop(context);
            }
    });
  }); 
       }
    )
             
  
           
         
          ]
        ),
      ) 


  );

} 

 cargarmunisid(int idmunis) async {
    final munisBloc = Provider.munisBloc(context);
  await munisBloc.cargarMunisid(idmunis.toString());
  }
   
}