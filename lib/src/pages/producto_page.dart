

import 'dart:io';


import 'package:chatocenter/src/bloc/productos_bloc.dart';
import 'package:chatocenter/src/bloc/provider.dart';
import 'package:chatocenter/src/models/productos_model.dart';
import 'package:chatocenter/src/models/ventasdb_model.dart';
import 'package:chatocenter/src/providers/db_provider.dart';
import 'package:flutter/material.dart';




import 'package:chatocenter/src/utils/utils.dart' as utils;



import 'package:image_picker/image_picker.dart';



class ProductoPage extends StatefulWidget {
  
  @override
 
  _ProductoPageState createState() => _ProductoPageState();
}

class _ProductoPageState extends State<ProductoPage> {
  final formKey = GlobalKey<FormState>();
  final scaffoldKey= GlobalKey<ScaffoldState>();
   String dropdownValue = '1';
 
  ProductosBloc productoBloc;  //bloc implementar 
  ProductosModel productos = new ProductosModel();
  Ventasdb ventasdb = new Ventasdb();
  bool _guardando = false; 
  File foto;
  int boton = 0;
  int cantidad = 0; 
  String comentarios = "";
  @override
  Widget build(BuildContext context) {

    productoBloc = Provider.productoBloc(context);
   final ProductosModel prodData = ModalRoute.of(context).settings.arguments;  //verificar si vienen argumentos 
    if(prodData!=null){
      productos = prodData;
       boton = 1;
    }


    return Scaffold(
      key: scaffoldKey,
      
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text('Productos'),
        
       
      ),
      body: SingleChildScrollView(
       child: Container(
         padding: EdgeInsets.all(15.0),
         child: Form(
           key: formKey, // identificador unico de este form 
           child: Column(
              children: <Widget>[
                  _mostrarFoto(),
                  _crearNombre(),
                  _crearPrecio(),
                  _crearValor(),
                 _crearTipousu(),
                 _crearComment(),
                  _crearBoton(),

                   

                   
                  
                  
                
                 
          
              ]
             
           ))
       )
      ),
     
      
    );
  }

Widget _crearNombre()
{
return TextFormField(
  enabled: false,
  initialValue: productos.titulo, //valor inicial 
  textCapitalization: TextCapitalization.sentences,
  decoration: InputDecoration( 
    labelText: 'Titulo'
  ),
  onSaved: (value) => productos.titulo = value,
  validator: (value){
    if(value.length < 3 ){
      return 'Ingrese Titulo';
    }else{
      return null;
    }
  },
);
}
Widget _crearComment()
{
return TextFormField(
 
   
  textCapitalization: TextCapitalization.sentences,
  decoration: InputDecoration( 
    labelText: 'Comentario'
  ),
  onSaved: (value) => comentarios = value,
 
);
}
 
Widget _crearPrecio()
{

  return TextFormField(
     enabled: false,
    initialValue: productos.descripcion,
  textCapitalization: TextCapitalization.sentences,//para que acepte decimales en solo numeros 
  decoration: InputDecoration( 
    labelText: 'DESCRIPCION'
    
  ),
  onSaved: (value) => productos.descripcion = value,
  validator: (value){
     
   if(value.length < 3 ){
      return 'Ingrese descripcion';
    }else{
      return null;
    }

  },
);

 
}

  Widget _crearValor() {
    return TextFormField(
       enabled: false,
      initialValue: productos.precio.toString(),
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      decoration: InputDecoration(
        labelText: 'Precio'
      ),
      onSaved: (value) => productos.precio = double.parse(value),
      validator: (value) {

        if ( utils.isNumeric(value)  ) {
          return null;
        } else {
          return 'Sólo números';
        }

      },
    );
  }



Widget  _crearBoton(){
  return RaisedButton.icon(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
    color: Colors.black,
    textColor: Colors.white,
    onPressed:(_guardando)? null:_submit, icon: Icon(Icons.add_shopping_cart), label: Text('PEDIR')

    );
}
   _crearBotonDireccion(int boton, ProductosModel prodData){

    print("BOTONES");
    print(boton);
    if (boton==1) {
      
  
  
    return FloatingActionButton(
      backgroundColor: Colors.black,
      
      child: Icon(Icons.category, color: Colors.lightGreenAccent,),
      onPressed: ()=> Navigator.push(context, MaterialPageRoute(builder: (context) => ProductoPage(), settings: RouteSettings(arguments: prodData)),).then((value) {
  setState(() {
      // refresh state
    });
  }),
  
      );
        }
        else{
          return Container();
        }
    }
  

void _submit ()async{ // para disparar los mensajes 


if(!formKey.currentState.validate())return; //si es invalido manda error regresa true si es valido el formulario o false si no es valido

  formKey.currentState.save();

    productoBloc.editarProductos(productos);
  
 
  mostrarSnackbar('Guardado');
  print("titulos");
  print(productos.titulo);
  print(productos.id);
  ventasdb.idProductos = productos.id;
  ventasdb.producto = productos.descripcion;
  ventasdb.precio = productos.precio;
  ventasdb.cantidad = cantidad; 
  ventasdb.comentario = comentarios; 
    

  DBProvider.db.nuevaVenta(ventasdb);

 
 Navigator.pop(context);
 

}



void mostrarSnackbar(String mensaje){
/// mostrar mensaje al guardar 
  final snackbar = SnackBar(

    content: Text(mensaje),
    duration: Duration(milliseconds: 1500),
  );

  scaffoldKey.currentState.showSnackBar(snackbar);

}

Widget _mostrarFoto(){
  
  if(productos.img != "" && productos.img != " "){
    
    return FadeInImage(
      placeholder: AssetImage("assets/original.gif"), 
      image:   NetworkImage(productos.img),
      height: 300.0,
      fit: BoxFit.contain,
      );
  }
  else{
    return Image(
      image: AssetImage(foto?.path ??'assets/foragrop.png'),/// si tiene un valor lo pone si no pone la de default
      height: 300.00,
      fit: BoxFit.cover,
    );
  }
}


Widget _crearTipousu()
{
  
   
return DropdownButtonFormField<String>(
  isExpanded: true,
  
    value: dropdownValue,
    
    icon: Icon(Icons.arrow_downward),
    iconSize: 24,
    elevation: 16,
    style: TextStyle(
      color: Colors.black,
       
    ),
    
    
    onChanged: (String newValue) {
      
      setState(() {
       
          
      });
    },
    
    items: <String>['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25']
      .map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,        
          child: Text(value),
          
        );
      })
      .toList(),
      onSaved: (value) => cantidad = int.parse(value),
      //   onSaved: (value) => usuarios.idUsuariosTipos = idcategori ,
         
  );
}















_seleccionarFoto()async{
 foto = await ImagePicker.pickImage(
    source: ImageSource.gallery
    );

    if(foto != " "){
        productos.img = " ";
    }
    setState(() {
      
    });
}


}