
import 'package:chatocenter/src/pages/productosext_page.dart';
import 'package:chatocenter/src/providers/db_provider.dart';
import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';





import 'productos_page.dart';

void main() => runApp(MaterialApp(home: BottomNavBar()));

class BottomNavBar extends StatefulWidget {
  @override
  _BottomNavBarState createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {
  
  GlobalKey _bottomNavigationKey = GlobalKey();
final scaffoldKey= GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
       key: scaffoldKey,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.black,
        title: Text('ALMACARONE'),
       actions: <Widget>[
          
          IconButton(
            
            icon: Icon(Icons.delete_forever),
             onPressed:(){DBProvider.db.deleteallVentas();
              mostrarSnackbar('ELIMINADO'); 
              Navigator.pop(context);
              }, color: Colors.lightGreenAccent, iconSize: 40,),
            
            
        ],
      ),
        bottomNavigationBar: CurvedNavigationBar(
          key: _bottomNavigationKey,
          index: 0,
          height: 70.0,
          
          items: <Widget>[
             Icon(Icons.search, size: 30, color: Colors.lightGreenAccent, ),
      Icon(Icons.category, size: 30, color: Colors.lightGreenAccent),
  
          ],
        backgroundColor: Colors.lightGreenAccent,
    buttonBackgroundColor: Colors.black,
    color: Colors.black,
          animationDuration: Duration(milliseconds: 600),
          onTap: (index) {
            setState(() {
             _showPage = _callPage(index);
            });
          },
        ),
        body: Container(
          
          child: Center(
            child: _showPage,
          ),
        ));
  }
  Widget _callPage(int paginaActual){
    

  switch(paginaActual){
  
    case 0: return ProductosPage();
    case 1: return ProductosextPage();
   
    

    default: 
    return ProductosPage();
    
  }
}
void mostrarSnackbar(String mensaje){
/// mostrar mensaje al guardar 
  final snackbar = SnackBar(
    backgroundColor: Colors.redAccent,
    content: Text(mensaje),
    duration: Duration(milliseconds: 500),
  );

  scaffoldKey.currentState.showSnackBar(snackbar);

}

Widget _showPage = new ProductosPage();


}