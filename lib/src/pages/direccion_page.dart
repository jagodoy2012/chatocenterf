 

import 'dart:io';

import 'package:chatocenter/src/bloc/provider.dart';
import 'package:chatocenter/src/bloc/usuariosdirecciones_bloc.dart';
import 'package:chatocenter/src/models/colonias_model.dart';
import 'package:chatocenter/src/models/usuariosdirecciones_model.dart';
import 'package:flutter/material.dart';







class DireccioPage extends StatefulWidget {
  
  @override
  _DireccioPageState createState() => _DireccioPageState();
}

class _DireccioPageState extends State<DireccioPage> {
  final formKey = GlobalKey<FormState>();
  final scaffoldKey= GlobalKey<ScaffoldState>();
   String dropdownValue = '1,Cliente'; // primer valor
 
  UsuariosDireccionesBloc direccionBloc;  //bloc implementar 
  UsuariosDireccionesModel direcciones = new UsuariosDireccionesModel();
  bool _guardando = false; 
  File foto;
  int idcategori=0;
  int boton = 0; 
  @override
  Widget build(BuildContext context) {

    direccionBloc = Provider.usuariosDireccionesBloc(context);
   final ColoniasModel prodData = ModalRoute.of(context).settings.arguments;  //verificar si vienen argumentos 
   
 
    if(prodData!=null){
      direcciones.id_colonia_sucursal = prodData.idSucursal;
       boton = 1; 
    }

    
    return Scaffold(
      key: scaffoldKey,
   
      appBar: AppBar(
       
        backgroundColor: Colors.black,
        title: Text('Direccion'),
        
        
      ),
      body: SingleChildScrollView(
       child: Container(
         padding: EdgeInsets.all(15.0),
         child: Form(
           key: formKey, // identificador unico de este form 
           child: Column(
              children: <Widget>[
                 //_mostrarFoto(),
                  _crearNombre(),
                 // _crearApellido(),
                  //_crearEmail(),
                  //_crearPrecio(),
                  //_crearTipousu(prodData),
                 
                  _crearBoton()

                     
                  
                  
                
                 
          
              ]
             
           ))
       )
      ),
     
      
    );
  }

Widget _crearNombre()
{
return TextFormField(
  
  textCapitalization: TextCapitalization.sentences,
  decoration: InputDecoration( 
    labelText: 'Direccion'
  ),
  onSaved: (value) => direcciones.direccion = value,
  validator: (value){
    if(value.length < 3 ){
      return 'Ingrese Direccion';
    }else{
      return null;
    }
  },
);
}





Widget  _crearBoton(){
  return RaisedButton.icon(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
    color: Colors.black,
    textColor: Colors.white,
    onPressed:(_guardando)? null:_submit, icon: Icon(Icons.save), label: Text('Guardar')

    );
}

  

void _submit ()async{ // para disparar los mensajes 


if(!formKey.currentState.validate())return; //si es invalido manda error regresa true si es valido el formulario o false si no es valido

  formKey.currentState.save();
    // para bloquear boton de guardar 

  setState(() {
   _guardando = true; //para redibujar widget  
  });
  

 
 
  if(direcciones.id == null){
    direccionBloc.agregarUsuariosDirecciones(direcciones);
    print("ID ID");
  }
  else{
    direccionBloc.editarUsuariosDirecciones(direcciones);
    print("NO ID");
  }

   
  mostrarSnackbar('Guardado');

     Navigator.pop(context);
   

 
  

}



void mostrarSnackbar(String mensaje){
/// mostrar mensaje al guardar 
  final snackbar = SnackBar(

    content: Text(mensaje),
    duration: Duration(milliseconds: 1500),
  );

  scaffoldKey.currentState.showSnackBar(snackbar);

}



}