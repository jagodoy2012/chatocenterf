
import 'package:chatocenter/src/bloc/provider.dart';
import 'package:chatocenter/src/bloc/usuarios_bloc.dart';
import 'package:chatocenter/src/models/productos_model.dart';
import 'package:chatocenter/src/models/usuarios_model.dart';
import 'package:chatocenter/src/pages/direcciones_page.dart';
import 'package:chatocenter/src/pages/usuario_page.dart';
import 'package:chatocenter/src/preferencias_usuario/preferencias_usuario.dart';
import 'package:chatocenter/src/providers/usuarios_provider.dart';
import 'package:flutter/material.dart';






class UsuariosPage extends StatefulWidget {
//final categoriasModel = CategoriasProvider();
 //final categoriasProvider = new CategoriasProvider();
  @override
  _UsuariosPageState createState() => _UsuariosPageState();
}

class _UsuariosPageState extends State<UsuariosPage> {
void initState() {
    super.initState();
   
  }
  final usuar = new UsuariosProvider();
   int tel = 0;
    final  usuariotel = new UsuariosModel();
  @override  
    
  Widget build(BuildContext context) {
final ProductosModel prodData = ModalRoute.of(context).settings.arguments;  
 if(prodData!=null){
      
       tel = int.parse(prodData.titulo);
    }


 usuariotel.celular = tel.toString();   
    final usuariosBloc = Provider.usuariosBloc(context);
    usuariosBloc.cargarTelUsuarios(tel);


       return Scaffold(
           appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text('CLIENTES'),
       
      ), 
      body:  
      
            
                    _crearListado(usuariosBloc),
                
                    
            
      
      floatingActionButton: _crearBoton(context),
      
 
       );
  }

  Widget _crearListado( UsuariosBloc usuariosBloc){ 


    
        return  
        RefreshIndicator(
                onRefresh:  (){

                return  usuariosBloc.cargarTelUsuarios(tel);
                },
          child: StreamBuilder(
        stream: usuariosBloc.usuariosStream,
        
        builder: (BuildContext context, AsyncSnapshot<List<UsuariosModel>> snapshot){
          final usuario  = snapshot.data;
          if(snapshot.hasData){
            return  
            
            ListView.builder(
            itemCount: usuario.length,
            itemBuilder: (context, i) => _crearItem(context, usuario[i], usuariosBloc),
            );
          } 
          else{
            return Center(child: CircularProgressIndicator(),);
          }
          
        },
      ),
    );

   

  }

Widget _crearItem(BuildContext context, UsuariosModel usuarios, UsuariosBloc usuariosBloc){

  return Dismissible(
      key: UniqueKey(),
      background: Container(
        color: Colors.red,
        child: Icon(Icons.delete,color: Colors.white,size: 50.0,),
        alignment: Alignment.centerLeft,
      ),
       secondaryBackground: Container(
        child: Icon(Icons.thumb_up, color: Colors.white, size: 50.0,),
        color: Colors.lightGreenAccent,
        alignment: Alignment.centerRight,
      ),
      onDismissed: (DismissDirection dir){
        // Remove the dismissed item from the list
  
    
    
    if (dir == DismissDirection.startToEnd) {
     usuariosBloc.borrarUsuarios(usuarios);
    
    } else {
  setState(() {
      // Navigator.push(context, MaterialPageRoute(builder: (context) => UsuariosAsigCliePage(), settings: RouteSettings(arguments: usuarios))).then((value) {
  
 
     // return true;
     
      // refresh state
    
   //});
  });
    }
    
        //setState(()=> usuariosBloc.borrarUsuarios(usuarios));
        //categoriasProvider.borrarCategoria(categorias);
       // usuariosBloc.borrarUsuarios(usuarios);
      
      },
     
      child: Card( 
        child:  Column(
          children: <Widget>[ 
          
           
            ListTile(
     leading: Icon(Icons.location_city), 
      title:  Text('Nombre: ${usuarios.nombre}'' ''${usuarios.apellido}'),
      subtitle: Text('Telefono: ${usuarios.celular}  Nit: ${usuarios.inst} '),
      onTap: () {
         final _prefs = new PreferenciasUsuario();
            _prefs.idcliente = usuarios.id;
      Navigator.push(context, MaterialPageRoute(builder: (context) => DireccionesPage(), settings: RouteSettings(arguments: usuarios))).then((value) {
  setState(() {
            
      // refresh state
        Navigator.pop(context);
    });
  }); 
     }  
    ),
  
          ] 
        ),
      ) 


  );
 
}

  _crearBoton(BuildContext context){
    return FloatingActionButton(
      backgroundColor: Colors.black,
      
      child: Icon(Icons.add, color: Colors.lightGreenAccent,),
      
      onPressed: ()=> Navigator.push(context, MaterialPageRoute(builder: (context) => UsuarioPage(), settings: RouteSettings(arguments: usuariotel))).then((value) {
  setState(() {
      // refresh state
      
    });
  }),
  
      );
    }

   
}