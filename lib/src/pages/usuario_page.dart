

import 'dart:io';

import 'package:chatocenter/src/bloc/provider.dart';
import 'package:chatocenter/src/bloc/usuarios_bloc.dart';
import 'package:chatocenter/src/models/usuarios_model.dart';
import 'package:chatocenter/src/models/usuariosnpass_modal.dart';
import 'package:chatocenter/src/pages/direcciones_page.dart';
import 'package:flutter/material.dart';



import 'package:image_picker/image_picker.dart';


class UsuarioPage extends StatefulWidget {
  
  @override
  _UsuarioPageState createState() => _UsuarioPageState();
}

class _UsuarioPageState extends State<UsuarioPage> {
  final formKey = GlobalKey<FormState>();
  final scaffoldKey= GlobalKey<ScaffoldState>();
   String dropdownValue = '1,Cliente'; // primer valor
 
  UsuariosBloc usuariosBloc;  //bloc implementar 
  UsuariosModel usuarios = new UsuariosModel();
  UsuariosNPassModel usariosnpaas = new UsuariosNPassModel();
  bool _guardando = false; 
  File foto;
  int idcategori=0;
  int boton = 0;
  @override
  Widget build(BuildContext context) {

    usuariosBloc = Provider.usuariosBloc(context);
   final UsuariosModel prodData = ModalRoute.of(context).settings.arguments;  //verificar si vienen argumentos 
    
    if(prodData!=null){
      usuarios = prodData;
    




       boton = 1;
    }

    
    return Scaffold(
      key: scaffoldKey,
   
      appBar: AppBar(
       
        backgroundColor: Colors.black,
        title: Text('Usuarios'),
        
        actions: 
        
        <Widget>[
            

          IconButton(

            icon: Icon(Icons.add_photo_alternate),
             onPressed:_seleccionarFoto, color: Colors.lightGreenAccent, iconSize: 40,),
            
            
        ],
      ),
      body: SingleChildScrollView(
       child: Container(
         padding: EdgeInsets.all(15.0),
         child: Form(
           key: formKey, // identificador unico de este form 
           child: Column(
              children: <Widget>[
                 _mostrarFoto(),
                  _crearNombre(),
                  _crearApellido(),
                  _crearCelular(),
                  _crearPrecio(),
                  _crearNit(),
                
                  
                   _crearBoton()
                 
                  
                  
                
                  
          
              ]
             
           ))
       )
      ),
     
      
    );
  }

Widget _crearNombre()
{
return TextFormField(
  initialValue: usuarios.nombre, //valor inicial 
  textCapitalization: TextCapitalization.sentences,
  decoration: InputDecoration( 
    labelText: 'Nombre'
  ),
  onSaved: (value) => usuarios.nombre = value,

);
}
Widget _crearApellido()
{
return TextFormField(
  initialValue: usuarios.apellido, //valor inicial 
  textCapitalization: TextCapitalization.sentences,
  decoration: InputDecoration( 
    labelText: 'Apellido'
  ),
  onSaved: (value) => usuarios.apellido = value,
  validator: (value){
    if(value.length < 3 ){
      return 'Ingrese Apellido';
    }else{
      return null;
    }
  },
);
}
Widget _crearCelular()
{
return TextFormField(
  initialValue: usuarios.celular, //valor inicial 
  textCapitalization: TextCapitalization.sentences,
  decoration: InputDecoration( 
    labelText: 'Numero de telefono'
  ),
  onSaved: (value) => usuarios.celular = value,
  validator: (value){
    if(value.length < 8 ){
      return 'Ingrese Numero';
    }else{
      return null;
    }
  },
);
}
Widget _crearEmail()
{
return TextFormField(
  initialValue: usuarios.email, //valor inicial 
  textCapitalization: TextCapitalization.sentences,
  decoration: InputDecoration( 
    labelText: 'Email'
  ),
  onSaved: (value) => usuarios.email = value,
  validator: (value){
    if(value.length < 3 ){
      return 'Ingrese Email';
    }else{
      return null;
    }
  },
);
}
Widget _crearNit()
{
return TextFormField(
   //valor inicial 
  textCapitalization: TextCapitalization.sentences,
  decoration: InputDecoration( 
    labelText: 'Nit'
  ),
  onSaved: (value) => usuarios.inst = value,

);
}
Widget _crearTipousu(UsuariosModel prodData)
{
  
    if(prodData!=null){
      if (prodData.idUsuariosTipos==1) {
          dropdownValue = "1,Cliente";
      }
      else{
        dropdownValue = "2,STAFF";
      }
      
    }
return DropdownButtonFormField<String>(
  isExpanded: true,
  
    value: dropdownValue,
    
    icon: Icon(Icons.arrow_downward),
    iconSize: 24,
    elevation: 16,
    style: TextStyle(
      color: Colors.black,
       
    ),
    
    
    onChanged: (String newValue) {
      
      setState(() {
        dropdownValue = newValue;
        if (dropdownValue == "1,Cliente") {
          idcategori = 1;
        }
        else{
          idcategori = 2;
        }
          
      });
    },
    
    items: <String>['1,Cliente', '2,STAFF']
      .map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
          
        );
      })
      .toList(),
         
         onSaved: (value) => usuarios.idUsuariosTipos = idcategori ,
         
  );
}
Widget _crearPrecio()
{

  return TextFormField(
    
    initialValue: usuarios.descripcion,
  textCapitalization: TextCapitalization.sentences,//para que acepte decimales en solo numeros 
  decoration: InputDecoration( 
    labelText: 'DESCRIPCION'
    
  ),
  onSaved: (value) => usuarios.descripcion = value,
  validator: (value){
     
   if(value.length < 3 ){
      return 'Ingrese descripcion';
    }else{
      return null;
    }

  },
);

 
}



Widget _crearActivo(){
   bool catest = true;
  
 if(usuarios.estado == "1"){
      catest = true;
    }
    else{
      catest = false;
    }
  return SwitchListTile(

    
    value: catest, 
    title: Text('Estado'),
    activeColor: Colors.green,
    onChanged: (value) => setState((){
     if(value == true){
          usuarios.estado = '1'; 
        }
        else{
          usuarios.estado = '0'; 
        }
    })
    );


}

Widget  _crearBoton(){
  return RaisedButton.icon(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
    color: Colors.black,
    textColor: Colors.white,
    onPressed:(_guardando)? null:_submit, icon: Icon(Icons.save), label: Text('Guardar')

    );
}

  _crearBotonDireccion(int boton, UsuariosModel prodData){

    print("BOTONES");
    print(boton);
    if (boton==1) {
      
  
  
    return FloatingActionButton(
      backgroundColor: Colors.black,
      
      child: Icon(Icons.my_location, color: Colors.lightGreenAccent,),
      onPressed: ()=> Navigator.push(context, MaterialPageRoute(builder: (context) => DireccionesPage(), settings: RouteSettings(arguments: prodData)),).then((value) {
  setState(() {
      // refresh state
    });
  }),
  
      );
        }
        else{
          return Container();
        }
    }
  

void _submit ()async{ // para disparar los mensajes 


if(!formKey.currentState.validate())return; //si es invalido manda error regresa true si es valido el formulario o false si no es valido

  formKey.currentState.save();
    // para bloquear boton de guardar 

  setState(() {
   _guardando = true; //para redibujar widget  
  });
  
  if(foto!=null){
   usuarios.ubicacion = await usuariosBloc.subirFoto(foto);
  } 
 
 
  if(usuarios.id == null){
    usuariosBloc.agregarUsuarios(usuarios);
    print("ID ID");
  }
  else{ 

      usariosnpaas.id = usuarios.id;
      usariosnpaas.nombre = usuarios.nombre;
      usariosnpaas.apellido = usuarios.apellido;
      usariosnpaas.email = usuarios.correo;
      usariosnpaas.celular = usuarios.celular;
      usariosnpaas.idUsuariosTipos = usuarios.idUsuariosTipos;
      usariosnpaas.fb = usuarios.fb;
      usariosnpaas.inst = usuarios.inst;
      usariosnpaas.ubicacion = usuarios.ubicacion;
      usariosnpaas.descripcion = usuarios.descripcion;
      usariosnpaas.ubicacion = usuarios.ubicacion;
      usariosnpaas.estado = usuarios.estado;
    usuariosBloc.editarUsuarios(usariosnpaas);
    print("NO ID");
  }  

   
  mostrarSnackbar('Guardado');
   if(usuarios.id == null){

      // refresh state
      Navigator.pop(context);
    }
   else{
     Navigator.pop(context);
   }

 
  

}



void mostrarSnackbar(String mensaje){
/// mostrar mensaje al guardar 
  final snackbar = SnackBar(

    content: Text(mensaje),
    duration: Duration(milliseconds: 1500),
  );

  scaffoldKey.currentState.showSnackBar(snackbar);

}

Widget _mostrarFoto(){
  
  if(usuarios.ubicacion != "" && usuarios.ubicacion != " "){
     
    return FadeInImage(
      placeholder: AssetImage("assets/original.gif"), 
      image:   NetworkImage(usuarios.ubicacion),
      height: 300.0,
      fit: BoxFit.contain,
      );
  }
  else{
    return Image(
      image: AssetImage(foto?.path ??'assets/foragrop.png'),/// si tiene un valor lo pone si no pone la de default
      height: 300.00,
      fit: BoxFit.cover,
    );
  }
}

_seleccionarFoto()async{
 foto = await ImagePicker.pickImage(
    source: ImageSource.gallery
    );

    if(foto != " "){
        usuarios.ubicacion= " ";
    }
    setState(() {
      
    });
}


}