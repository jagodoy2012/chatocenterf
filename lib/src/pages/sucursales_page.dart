 import 'dart:ffi';






import 'package:chatocenter/src/bloc/mandados_bloc.dart';
import 'package:chatocenter/src/bloc/provider.dart';
import 'package:chatocenter/src/bloc/sucursales_bloc.dart';
import 'package:chatocenter/src/models/fact_model.dart';
import 'package:chatocenter/src/models/mandados_model.dart';
import 'package:chatocenter/src/models/sucursales_model.dart';
import 'package:chatocenter/src/models/usuariosdirecciones_model.dart';
import 'package:chatocenter/src/models/ventas_model.dart';
import 'package:chatocenter/src/pages/direccion_page.dart';
import 'package:chatocenter/src/preferencias_usuario/preferencias_usuario.dart';
import 'package:chatocenter/src/providers/db_provider.dart';
import 'package:chatocenter/src/providers/mandados_provider.dart';
import 'package:chatocenter/src/providers/ventas_provider.dart';
import 'package:flutter/material.dart';
 






class SucursalesPage extends StatefulWidget {
//final categoriasModel = CategoriasProvider();
 //final categoriasProvider = new CategoriasProvider();
final ventasProvider  = new VentasProvider();
  @override
  _SucursalesPageState createState() => _SucursalesPageState();
}
 UsuariosDireccionesModel direcciones = new UsuariosDireccionesModel();
 MandadosModel mandados = new MandadosModel();
 VentasModel ventas = new VentasModel();
  int idusuario = 0;
  String idusuariovent = "" ;
  String iddireccionvent = "" ;
  double totalvent = 0.00;
  Fact datosfac = new Fact();
  String nombr = ""; 
  String nit = "";
  String tel = "";
class _SucursalesPageState extends State<SucursalesPage> {
void initState() {
    super.initState();
   
  } 
  @override 
   
  Widget build(BuildContext context) { 

    final sucursalesBloc = Provider.sucursalesBloc(context);
     final mandadosBloc = Provider.mandadosBloc(context);
    
  final Fact prodData = ModalRoute.of(context).settings.arguments;  //verificar si vienen argumentos 
    if(prodData!=null){
      
      mandados.taxisMandadosDescripcion = prodData.envio;
      mandados.taxis_usuarios_pedir_direccion = prodData.direcc;
      mandados.taxisMandadosDescripcion = prodData.envio;
      idusuariovent = prodData.idusuario;
      iddireccionvent = prodData.iddirecc;
      totalvent = prodData.total;
    }


sucursalesBloc.cargarSucursales();



       return Scaffold(
         appBar: AppBar(
       
        backgroundColor: Colors.black,
        title: Text('SUCURSALES'),
        
        ),
      body:  _crearListado(sucursalesBloc, mandadosBloc), 
      //floatingActionButton: _crearBoton(context, idusuario),
      
    );
  }

  Widget _crearListado( SucursalesBloc sucursalesBloc, MandadosBloc mandadosBloc){ 


    return  
    RefreshIndicator(
            onRefresh: sucursalesBloc.cargarSucursales,
          child: StreamBuilder( 
        stream: sucursalesBloc.sucursalStream,
        
        builder: (BuildContext context, AsyncSnapshot<List<Sucursales>> snapshot){
          final sucursal  = snapshot.data;
          if(snapshot.hasData){
            return  
            
            ListView.builder(
            itemCount: sucursal.length,
            itemBuilder: (context, i) => _crearItem(context, sucursal[i], sucursalesBloc, mandadosBloc),
            );
          } 
          else{
            return Center(child: CircularProgressIndicator(),);
          }
          
        },
      ),
    );

   

  }

Widget _crearItem(BuildContext context, Sucursales sucursales, SucursalesBloc sucursalesBloc, MandadosBloc mandadosBloc){

  return Dismissible(
      key: UniqueKey(),
      background: Container(
        color: Colors.red,
      ),
     
      child: Card( 
        child:  Column(
          children: <Widget>[  
             
               new ListTile(
                 leading: Icon(Icons.location_city),
      title:  Text('Direccion: ${sucursales.taxisUsuariosDirecciones}'),
   
      onTap: () async { 
              
            mandados.taxisUsuariosPedirEstado = "1";
            mandados.taxisUsuariosPedirTaxisUsuariosDireccionesId = sucursales.taxis_usuarios_direcciones_id;
      
      print("pruebas de insert en tabla");
      final prefs = new PreferenciasUsuario();
      ventas.idUsuario = idusuariovent;
      ventas.idDirecciones = iddireccionvent;
      ventas.total = totalvent.toString();
      ventas.idAsesor =  prefs.idvendedor;
 
      prefs.returnpag = 0 ;
       
   
    

   int decodedData = await VentasProvider().crearVentas(ventas); 
    mandados.taxis_usuarios_pedir_taxis_usuarios_id = decodedData;
 
  
    MandadosProvider().crearMandad(mandados);
  
    DBProvider.db.deleteallVentas();
       
     Navigator.pop(context);
   
    
       }
    )
             
  
           
         
          ]
        ),
      ) 


  );

}

  _crearBoton(BuildContext context, int idusuario){
    return FloatingActionButton(
      
      backgroundColor: Colors.black, 
      
      child: Icon(Icons.add, color: Colors.lightGreenAccent,),
      onPressed: () {
        direcciones.idUsuarios = idusuario;
        Navigator.push(context, MaterialPageRoute(builder: (context) => DireccioPage(), settings: RouteSettings(arguments: direcciones))).then((value) {
  setState(() {
      // refresh state
    });
  });}
  
      );
    }

   
}