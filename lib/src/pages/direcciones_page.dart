
import 'package:chatocenter/src/bloc/provider.dart';
import 'package:chatocenter/src/bloc/usuariosdirecciones_bloc.dart';
import 'package:chatocenter/src/models/fact_model.dart';
import 'package:chatocenter/src/models/usuarios_model.dart';
import 'package:chatocenter/src/models/usuariosdirecciones_model.dart';
import 'package:chatocenter/src/pages/BottomNavBar.dart';
import 'package:chatocenter/src/pages/depto_page.dart';
import 'package:chatocenter/src/pages/direccion_page.dart';
import 'package:chatocenter/src/preferencias_usuario/preferencias_usuario.dart';
import 'package:flutter/material.dart';

 







class DireccionesPage extends StatefulWidget {
//final categoriasModel = CategoriasProvider();
 //final categoriasProvider = new CategoriasProvider();

  @override
  _DireccionesPageState createState() => _DireccionesPageState();
}
 UsuariosDireccionesModel direcciones = new UsuariosDireccionesModel();
  int idusuario = 0;
  Fact datosfac = new Fact();
  String nombr = "";
  String nit = "";
  String tel = "";
final _prefs = new PreferenciasUsuario();
class _DireccionesPageState extends State<DireccionesPage> {
void initState() {
    super.initState();
   
  }
  @override 
  
  Widget build(BuildContext context) { 

    final direccionesBloc = Provider.usuariosDireccionesBloc(context);
    
  final UsuariosModel prodData = ModalRoute.of(context).settings.arguments;  //verificar si vienen argumentos 
    if(prodData!=null){
        
      idusuario = prodData.id;
      nombr = prodData.nombre +" "+ prodData.apellido;
      nit = prodData.inst;
      tel = prodData.celular;
   
    }
 //variable de sesion dinamica

direccionesBloc.cargarUsuariosDireccionesid(idusuario.toString());
       return Scaffold(
         appBar: AppBar(
       
        backgroundColor: Colors.black,
        title: Text('Direcciones'),
        
        ),
      body:  _crearListado(direccionesBloc),
      floatingActionButton: _crearBoton(context, idusuario),
      
    );
  }

  Widget _crearListado( UsuariosDireccionesBloc direccionesBloc){ 


    return  
    RefreshIndicator(
            onRefresh:  (){

                return  direccionesBloc.cargarUsuariosDireccionesid(idusuario.toString());
                }, 
                 
          child: StreamBuilder(
        stream: direccionesBloc.usuariosdireccionesStream,
        
         
        builder: (BuildContext context, AsyncSnapshot<List<UsuariosDireccionesModel>> snapshot){
          final direccion  = snapshot.data;
          if(snapshot.hasData){
            return  
            
            ListView.builder(
            itemCount: direccion.length,
            itemBuilder: (context, i) => _crearItem(context, direccion[i], direccionesBloc),
            );
          } 
          else{
            return Center(child: CircularProgressIndicator(),);
          }
          
        },
      ),
    );

   

  }

Widget _crearItem(BuildContext context, UsuariosDireccionesModel direcciones, UsuariosDireccionesBloc direccionesBloc){

  return Dismissible(
      key: UniqueKey(),
      background: Container(
        color: Colors.red,
      ),
      onDismissed: (direccion){
        //categoriasProvider.borrarCategoria(categorias);
        direccionesBloc.borrarUsuariosDirecciones(direcciones);
          
      },
      child: Card( 
        child:  Column(
          children: <Widget>[  
            
               new ListTile(
                 leading: Icon(Icons.location_city),
      title:  Text('Direccion: ${direcciones.direccion}'),
   
      onTap: () {
            final prefs = new PreferenciasUsuario();
            datosfac.direcc  = direcciones.direccion;
            datosfac.iddirecc = direcciones.id.toString();
            datosfac.nombre = nombr;
            datosfac.nit = nit; 
            datosfac.idusuario = idusuario.toString();
            datosfac.tel = tel;
            prefs.idsucursal = direcciones.id_colonia_sucursal;

      Navigator.push(context, MaterialPageRoute(builder: (context) => BottomNavBar(), settings: RouteSettings(arguments: datosfac))).then((value) {
  setState(() {
  
       // refresh state 
        Navigator.pop(context); 
    });
  }); 
       }
    )
              
  
           
         
          ]
        ),
      ) 


  );

} 

  _crearBoton(BuildContext context, int idusuario){
    _prefs.returndir = 1;
    return FloatingActionButton(
      
      backgroundColor: Colors.black, 
      
      child: Icon(Icons.add, color: Colors.lightGreenAccent,),
      onPressed: () {
        direcciones.idUsuarios = idusuario;
        Navigator.push(context, MaterialPageRoute(builder: (context) => DeptoPage())).then((value) {
  setState(() {
      // refresh state
    });
  });}
  
      );
    }

   
}