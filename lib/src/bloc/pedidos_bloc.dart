 




import 'package:chatocenter/src/models/pedidos_model.dart';

import 'package:chatocenter/src/providers/pedidos_provider.dart';

import 'package:rxdart/subjects.dart';
 
class PedidosBloc{

final _pedidosController = new BehaviorSubject<List<PedidosCliente>>();
final _cargandoController = new BehaviorSubject<bool>();

final _pedidosProvider = new PedidosProvider();


Stream<List<PedidosCliente>> get pedidosStream => _pedidosController.stream;

Stream<bool> get cargando => _cargandoController.stream;


Future<Null> cargarTelUsuarios(int tel)async{
print("cargargar usuarios por cel");
final usuarios = await _pedidosProvider.cargarTelUsuarios(tel);
_pedidosController.sink.add(usuarios);

}




dispose(){
_pedidosController?.close();
_cargandoController?.close();

}


}