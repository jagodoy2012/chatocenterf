
import 'dart:io';


import 'package:chatocenter/src/models/productos_model.dart';
import 'package:chatocenter/src/providers/productos_provider.dart';
import 'package:rxdart/subjects.dart';
 
class ProductosBloc{

final _productosController = new BehaviorSubject<List<ProductosModel>>();
final _cargandoController = new BehaviorSubject<bool>();

final _productosProvider = new ProductosProvider();

Stream<List<ProductosModel>> get productosStream => _productosController.stream;

Stream<bool> get cargando => _cargandoController.stream;

Future<Null> cargarProductos()async{
print("cargargar categorias");
final productos = await _productosProvider.cargarProductos();
_productosController.sink.add(productos);

}
Future<Null> cargarProductosext()async{
print("cargargar categorias");
final productos = await _productosProvider.cargarProductosext();
_productosController.sink.add(productos);

}
Future<Null> cargarbdProductos()async{
print("cargargar categorias");
final productos = await _productosProvider.cargarProductos();
_productosController.sink.add(productos);

}

 agregarProductos(ProductosModel productos)async{
 
 _cargandoController.sink.add(true);
 await _productosProvider.crearProductos(productos);
 _cargandoController.sink.add(false);

}
//void cuando no regresa nada 
Future subirFoto(File foto)async{

 _cargandoController.sink.add(true);
 final fotoUrl = await _productosProvider.subirImagen(foto);
 _cargandoController.sink.add(false);
  return fotoUrl;
}

void editarProductos(ProductosModel productos)async{
 
 _cargandoController.sink.add(true);
 await _productosProvider.modificarProductos(productos);
 _cargandoController.sink.add(false);

}
void borrarProductos(ProductosModel productos)async{


  _cargandoController.sink.add(true);
 await _productosProvider.modificarProductos(productos);
 _cargandoController.sink.add(false);


}




dispose(){
_productosController?.close();
_cargandoController?.close();

}


}