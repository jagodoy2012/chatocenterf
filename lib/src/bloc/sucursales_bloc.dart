 
import 'dart:io';



import 'package:chatocenter/src/models/sucursales_model.dart';
import 'package:chatocenter/src/providers/sucursales_provider.dart';
import 'package:rxdart/subjects.dart';
 
class SucursalesBloc{

final _sucursalController = new BehaviorSubject<List<Sucursales>>();
final _cargandoController = new BehaviorSubject<bool>();

final _sucursalProvider = new SucursalesProvider();


Stream<List<Sucursales>> get sucursalStream =>  _sucursalController.stream;

Stream<bool> get cargando => _cargandoController.stream;

Future<Null> cargarSucursales()async{
print("cargargar categorias");
final sucursal = await _sucursalProvider.cargarSucursales();
_sucursalController.sink.add(sucursal);

}

dispose(){
_sucursalController?.close();
_cargandoController?.close();

}


}