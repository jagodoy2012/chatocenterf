   
import 'dart:io';



import 'package:chatocenter/src/models/usuariosdirecciones_model.dart';
import 'package:chatocenter/src/providers/usuariosdirecciones_provider.dart';
import 'package:rxdart/subjects.dart';
 
class UsuariosDireccionesBloc{

final _usuariosDireccionesController = new BehaviorSubject<List<UsuariosDireccionesModel>>();
final _cargandoController = new BehaviorSubject<bool>();

final _usuariosDireccionesProvider = new UsuariosDireccionesProvider();

Stream<List<UsuariosDireccionesModel>> get usuariosdireccionesStream => _usuariosDireccionesController.stream;

Stream<bool> get cargando => _cargandoController.stream;

Future<Null> cargarUsuariosDirecciones()async{

final usuariosDirecciones = await _usuariosDireccionesProvider.cargarUsuariosDirecciones();
_usuariosDireccionesController.sink.add(usuariosDirecciones);

}
Future<Null> cargarUsuariosDireccionesid(String iddir)async{

final usuariosDirecciones = await _usuariosDireccionesProvider.cargarUsuariosDireccionesid(iddir);
_usuariosDireccionesController.sink.add(usuariosDirecciones);

}

 agregarUsuariosDirecciones(UsuariosDireccionesModel usuariosDirecciones)async{
 
 _cargandoController.sink.add(true);
 await _usuariosDireccionesProvider.crearUsuariosDirecciones(usuariosDirecciones);
 _cargandoController.sink.add(false);

}
//void cuando no regresa nada 
Future subirFoto(File foto)async{

 _cargandoController.sink.add(true);
 final fotoUrl = await _usuariosDireccionesProvider.subirImagen(foto); 
 _cargandoController.sink.add(false);
  return fotoUrl;
}

void editarUsuariosDirecciones(UsuariosDireccionesModel usuariosDirecciones)async{
 
 _cargandoController.sink.add(true);
 await _usuariosDireccionesProvider.modificarUsuariosDirecciones(usuariosDirecciones);
 _cargandoController.sink.add(false);

}
void borrarUsuariosDirecciones(UsuariosDireccionesModel usuariosDirecciones)async{


  _cargandoController.sink.add(true);
 await _usuariosDireccionesProvider.modificarUsuariosDirecciones(usuariosDirecciones);
 _cargandoController.sink.add(false);


}




dispose(){
_usuariosDireccionesController?.close();
_cargandoController?.close();

}


} 