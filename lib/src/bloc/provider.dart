

import 'package:chatocenter/src/bloc/colonias_bloc.dart';
import 'package:chatocenter/src/bloc/depto_bloc.dart';
import 'package:chatocenter/src/bloc/mandados_bloc.dart';
import 'package:chatocenter/src/bloc/munis_bloc.dart';
import 'package:chatocenter/src/bloc/pedidos_bloc.dart';
import 'package:chatocenter/src/bloc/productos_bloc.dart';
import 'package:chatocenter/src/bloc/sucursales_bloc.dart';
import 'package:chatocenter/src/bloc/usuarios_bloc.dart';
import 'package:chatocenter/src/bloc/usuariosdirecciones_bloc.dart';
import 'package:chatocenter/src/bloc/zonas_bloc.dart';
import 'package:flutter/material.dart';


import 'login_bloc.dart';


 
class Provider extends InheritedWidget {


   final _productosBloc = new ProductosBloc();
   
     final _usuariosBloc = new UsuariosBloc(); 
  
     final _usuariosDireccionesBloc = new UsuariosDireccionesBloc(); 
      final _sucursalesBloc = new SucursalesBloc(); 
      final _mandadosBloc = new MandadosBloc(); 
       final _loginBloc = LoginBloc();
      
  
      final _coloniasBloc = ColoniasBloc();
      final _zonasBloc = ZonasBloc();
      final _munisBloc = MunisBloc();
      final _deptoBloc = DeptoBloc();
      final _pedidosBloc = PedidosBloc();
    


  static Provider _instancia;

  factory Provider({ Key key, Widget child }) {

    if ( _instancia == null ) {
      _instancia = new Provider._internal(key: key, child: child );
    }
     
  
    return _instancia;

  }

  Provider._internal({ Key key, Widget child })
    : super(key: key, child: child );




  // Provider({ Key key, Widget child })
  //   : super(key: key, child: child );

 
  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  
  static ProductosBloc productoBloc ( BuildContext context ) {
    return context.dependOnInheritedWidgetOfExactType<Provider>()._productosBloc;
  }
 
    static UsuariosBloc usuariosBloc ( BuildContext context ) {
    return context.dependOnInheritedWidgetOfExactType<Provider>()._usuariosBloc;
  }
   
    static UsuariosDireccionesBloc usuariosDireccionesBloc ( BuildContext context ) {
    return context.dependOnInheritedWidgetOfExactType<Provider>()._usuariosDireccionesBloc;
  }
  static SucursalesBloc sucursalesBloc ( BuildContext context ) {
    return context.dependOnInheritedWidgetOfExactType<Provider>()._sucursalesBloc;
  }
    static MandadosBloc mandadosBloc ( BuildContext context ) {
    return context.dependOnInheritedWidgetOfExactType<Provider>()._mandadosBloc;
  }
 static LoginBloc loginBloc ( BuildContext context ) {
    return  context.dependOnInheritedWidgetOfExactType<Provider>()._loginBloc;
  }


 static DeptoBloc deptoBloc ( BuildContext context ) {
    return  context.dependOnInheritedWidgetOfExactType<Provider>()._deptoBloc;
  }
   static MunisBloc munisBloc ( BuildContext context ) {
    return  context.dependOnInheritedWidgetOfExactType<Provider>()._munisBloc;
  }
  static ZonasBloc zonasBloc ( BuildContext context ) {
    return  context.dependOnInheritedWidgetOfExactType<Provider>()._zonasBloc;
  }
    static ColoniasBloc coloniasBloc ( BuildContext context ) {
    return  context.dependOnInheritedWidgetOfExactType<Provider>()._coloniasBloc;
  }
   static PedidosBloc pedidosBloc ( BuildContext context ) {
    return  context.dependOnInheritedWidgetOfExactType<Provider>()._pedidosBloc;
  }
  
  static of(BuildContext context) {}



}