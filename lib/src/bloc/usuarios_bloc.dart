 
import 'dart:io';



import 'package:chatocenter/src/models/usuarios_model.dart';
import 'package:chatocenter/src/models/usuariosnpass_modal.dart';
import 'package:chatocenter/src/providers/usuarios_provider.dart';
import 'package:rxdart/subjects.dart';
 
class UsuariosBloc{

final _usuariosController = new BehaviorSubject<List<UsuariosModel>>();
final _cargandoController = new BehaviorSubject<bool>();

final _usuariosProvider = new UsuariosProvider();
final _usuariosnpassProvider = new UsuariosProvider();

Stream<List<UsuariosModel>> get usuariosStream => _usuariosController.stream;

Stream<bool> get cargando => _cargandoController.stream;

Future<Null> cargarUsuarios()async{
print("cargargar categorias");
final usuarios = await _usuariosProvider.cargarUsuarios();
_usuariosController.sink.add(usuarios);

}
Future<Null> cargarTelUsuarios(int tel)async{
print("cargargar usuarios por cel");
final usuarios = await _usuariosProvider.cargarTelUsuarios(tel);
_usuariosController.sink.add(usuarios);

}

 agregarUsuarios(UsuariosModel usuarios)async{
 
 _cargandoController.sink.add(true);
 await _usuariosProvider.crearUsuarios(usuarios);
 _cargandoController.sink.add(false);

}
//void cuando no regresa nada 
Future subirFoto(File foto)async{

 _cargandoController.sink.add(true);
 final fotoUrl = await _usuariosProvider.subirImagen(foto); 
 _cargandoController.sink.add(false);
  return fotoUrl;
}

void editarUsuarios(UsuariosNPassModel usuarios)async{
 
 _cargandoController.sink.add(true);
 await _usuariosnpassProvider.modificarUsuariosnpass(usuarios);
 _cargandoController.sink.add(false);

}
void borrarUsuarios(UsuariosModel usuarios)async{


  _cargandoController.sink.add(true);
 await _usuariosProvider.modificarUsuarios(usuarios);
 _cargandoController.sink.add(false);


}




dispose(){
_usuariosController?.close();
_cargandoController?.close();

}


}