 
import 'dart:io';


import 'package:chatocenter/src/models/mandados_model.dart';
import 'package:chatocenter/src/providers/mandados_provider.dart';
import 'package:rxdart/subjects.dart';
 
class MandadosBloc{

final _mandadosController = new BehaviorSubject<List<MandadosModel>>();
final _cargandoController = new BehaviorSubject<bool>();

final _mandadoProvider = new MandadosProvider();

Stream<List<MandadosModel>> get productosStream => _mandadosController.stream;

Stream<bool> get cargando => _cargandoController.stream;



 agregarMandados(MandadosModel mandados)async{
 
 _cargandoController.sink.add(true);
 await _mandadoProvider.crearMandad(mandados);
 _cargandoController.sink.add(false);

}
//void cuando no regresa nada 




  

dispose(){
_mandadosController?.close();
_cargandoController?.close();

}


}